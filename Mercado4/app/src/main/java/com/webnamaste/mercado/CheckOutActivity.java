package com.webnamaste.mercado;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.webnamaste.database.DatabaseHandler;
import com.webnamaste.database.SessionManager;
import com.webnamaste.pojo.Address;
import com.webnamaste.pojo.CartItem;
import com.webnamaste.pojo.HistoryOrder;
import com.webnamaste.pojo.Item;
import com.webnamaste.pojo.Items;
import com.webnamaste.pojo.JsonUtil;
import com.webnamaste.pojo.OrderForm;
import com.webnamaste.pojo.OrderHeader;
import com.webnamaste.pojo.OrderItem;
import com.webnamaste.pojo.OrderShop;
import com.webnamaste.pojo.OrderToSave;
import com.webnamaste.pojo.OrderUser;
import com.webnamaste.pojo.ProductPrice;
import com.webnamaste.pojo.Status;
import com.webnamaste.pojo.User;
import com.webnamaste.singletons.StoriCart;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.transform.ErrorListener;


public class CheckOutActivity extends AppCompatActivity {

    public static final String URL_STORICART_ORDER_RETRIEVAL = "http://storicart.com/services/ordersbyuser?";

    SessionManager sessionManager;
    DatabaseHandler databaseHandler;

    EditText etAddressLine1, etAddressLine2, etStreet, etCity, etState, etCountry, etZip;
    Button buttonConfirm, buttonSignIn;

    ProgressBar progressBar;
    RequestQueue requestQueue;

    Address newAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        createJsonClasses();
        sessionManager = new SessionManager(StoriCart.getAppContext());
        if (sessionManager.isLoggedIn()) {
            setDelivery();
        } else {
            buttonSignIn = (Button) findViewById(R.id.buttonSignIn);
            buttonSignIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(CheckOutActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            });
            Intent intent = new Intent(CheckOutActivity.this, LoginActivity.class);
            startActivity(intent);
        }


    }

    private void setDelivery() {
        setContentView(R.layout.activity_checkout_delivery);
        User user = sessionManager.getUserDetails();
        Address address = user.getAddress();
        etAddressLine1 = (EditText) findViewById(R.id.etAddressLine1);
        etAddressLine2 = (EditText) findViewById(R.id.etAddressLine2);
        etStreet = (EditText) findViewById(R.id.etStreet);
        etCity = (EditText) findViewById(R.id.etCity);
        etState = (EditText) findViewById(R.id.etState);
        etCountry = (EditText) findViewById(R.id.etCountry);
        buttonConfirm = (Button) findViewById(R.id.buttonConfirm);
        etZip = (EditText) findViewById(R.id.etZip);

        etAddressLine1.setText(address.getAddressLine1());
        etAddressLine2.setText(address.getAddressLine2());
        etStreet.setText(address.getStreet());
        etCity.setText(address.getCity());
        etState.setText(address.getState());
        etCountry.setText(address.getCountry());
        etZip.setText(address.getZip());

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //newAddress = new Address(etAddressLine1.getText().toString(), etAddressLine2.getText().toString(), etStreet.getText().toString(), etCity.getText().toString(), etState.getText().toString(), etCountry.getText().toString(), etZip.getText().toString());
                if (newAddress.validateAddress()) {
                    gotoPayment();
                }
            }
        });

    }

    private void gotoPayment() {
        setContentView(R.layout.activity_checkout_payment);
        Button buttonProceed = (Button) findViewById(R.id.buttonProceed);
        buttonProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeOrder();
            }
        });
    }

    private boolean makeOrder() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        ArrayList<Integer> shopIds;
        databaseHandler = new DatabaseHandler(StoriCart.getAppContext());
        shopIds = databaseHandler.getCartShopIds();
        ArrayList<OrderToSave> order = new ArrayList<OrderToSave>();
        for (Integer temp : shopIds) {
//            ArrayList<CartItem> cartItems = databaseHandler.getCartItemsFromShop(temp);
//            ArrayList<OrderItem> items = new ArrayList<OrderItem>();
//            OrderHeader orderHeader;
//            orderHeader = new OrderHeader();
//            orderHeader.setShipToAddress(newAddress);
//            User user = sessionManager.getUserDetails();
//            orderHeader.setUser(new OrderUser(user.getUserId(), user.getPassword(), user.getPhone()));
//            orderHeader.setAction("ADD");
//            orderHeader.setShop(new OrderShop(temp, databaseHandler.getShop(temp).getOrderSendToPhone()));
//            Calendar c = Calendar.getInstance();
//            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
//            orderHeader.setOrderedDate(df.format(c.getTime()));
//            orderHeader.setStatus(new Status("New order", "NEW"));
//            orderHeader.setOrderId(0);
//            orderHeader.setOrderType("ORDER");
//            OrderToSave orderToSave = new OrderToSave(orderHeader, new Items(items));
//            order.add(orderToSave);
            requestQueue = Volley.newRequestQueue(this);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, getRequestUrl(), null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObject) {
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                }
            });
            requestQueue.add(request);
        }
        progressBar.setVisibility(View.GONE);
        Toast.makeText(CheckOutActivity.this, "Your Order Has Been Placed", Toast.LENGTH_LONG).show();
        databaseHandler.clearCart();
        Intent intent;
        intent = new Intent(CheckOutActivity.this, ShopCategoriesActivity.class);
        intent.putExtra("latitude", 0);
        intent.putExtra("longitude", 0);
        intent.putExtra("isGPS", true);
        startActivity(intent);
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_check_out, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sessionManager.isLoggedIn()) {
            setDelivery();
        }
    }

    private String getRequestUrl() {
        return URL_STORICART_ORDER_RETRIEVAL + "json=" + 1 + "&userId=" + 1 + "&password=" + "123456";
    }


    private void createJsonClasses()
    {
        OrderHeader orderHeader = new OrderHeader();
        orderHeader.setAction("EDIT");
        orderHeader.setOrderId("1");
        orderHeader.setOrderType("ORDER");
        OrderUser orderUser = new OrderUser("1","123456","0987654321");
        OrderShop orderShop = new OrderShop("2","0987654321");
        Status orderstatus = new Status("Orderhasbeenacceptedinyourshop","ACCEPTED");



        newAddress = new Address(1,"adressline1", "addresslinetwo", "addressstreetroadname", "Kozhikode", "state", "India", "122312414");
        orderHeader.setShop(orderShop);
        orderHeader.setShipToAddress(newAddress);
        orderHeader.setUser(orderUser);
        orderHeader.setStatus(orderstatus);

        OrderToSave orderToSave = new OrderToSave();
        orderToSave.setHeader(orderHeader);


        OrderItem item = new OrderItem();
        item.setOrderItemId("3");
        item.setProductId(1);
        ProductPrice productPrice = new ProductPrice(50,"INR");
        item.setPrice(productPrice);

        List <OrderItem> itemList = new ArrayList<OrderItem>() ;
        itemList.add(0,item);

        orderToSave.setOrderItemList(itemList);


        List<OrderToSave>  orderToSaveList = new ArrayList<OrderToSave>();

        orderToSaveList.add(0, orderToSave);

        OrderForm orderForm = new OrderForm();
        orderForm.setOrderToSaveList(orderToSaveList);
        Log.v("LOG", "Post check out order " + JsonUtil.orderToJSon(orderForm));

        postOrderDataApi(JsonUtil.orderToJSon(orderForm));

       // System.out.println("Post check out order " +JsonUtil.orderToJSon(orderForm));

    }


    private void postOrderDataApi(String jsonRequest)
    {
        requestQueue = Volley.newRequestQueue(this);
        final ArrayList<HistoryOrder> results = new ArrayList<HistoryOrder>();

        JSONObject jsonRequestObj =null;
        try
        {
            jsonRequestObj = new JSONObject(jsonRequest);
        }catch (JSONException e)
        {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, getRequestUrlForOrder(), jsonRequestObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {

                    System.out.println("/////////RESPONSE///////////////");
                    System.out.println(jsonObject.toString());
                    //JSONArray ordersJSONArray = jsonObject.getJSONArray("order");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        requestQueue.add(request);
    }


    private String getRequestUrlForOrder() {
        return "http://storicart.com/services/saveorder";
    }
}
