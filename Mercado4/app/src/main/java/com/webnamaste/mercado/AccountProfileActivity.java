package com.webnamaste.mercado;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.webnamaste.pojo.Address;
import com.webnamaste.pojo.Details;
import com.webnamaste.pojo.Header;
import com.webnamaste.pojo.JsonUtil;
import com.webnamaste.pojo.Registration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AccountProfileActivity extends AppCompatActivity {

    private EditText editFirstName;
    private EditText editLastName ;
    private EditText editdateofbirth;
    private EditText editphonenumber ;
    private EditText editemail ;
    private EditText editadress1 ;
    private EditText editadress2 ;
    private EditText editstreet ;
    private EditText editCity ;
    private EditText editState;
    private EditText editCountry ;

    private EditText editpincode ;
    private Button submitButton ;

    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_profile);
        initialiseComponents();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_account_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void postRegistration(String jsonRequest)
    {
        requestQueue = Volley.newRequestQueue(this);

        JSONObject jsonRequestObj =null;
        try
        {
            jsonRequestObj = new JSONObject(jsonRequest);
        }catch (JSONException e)
        {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, "http://storicart.com/services/userregjson", jsonRequestObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {

                    System.out.println("///////RESPONSE REGISTRATIO///////////////");
                    System.out.println(jsonObject.toString());
                    //JSONArray ordersJSONArray = jsonObject.getJSONArray("order");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        requestQueue.add(request);
    }

    private void initialiseComponents()
    {
        submitButton = (Button)  findViewById(R.id.buttonConfirm);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitAllregistrationValues();
            }
        });
        editFirstName = (EditText) findViewById(R.id.etfirstname);
        editLastName = (EditText) findViewById(R.id.etlastname);
        editdateofbirth = (EditText) findViewById(R.id.etdob);
        editphonenumber = (EditText) findViewById(R.id.etphone);
        editemail = (EditText) findViewById(R.id.etemail);
        editstreet = (EditText) findViewById(R.id.etStreet);
        editCity = (EditText) findViewById(R.id.etCity);
        editState = (EditText) findViewById(R.id.etState);
        editCountry = (EditText) findViewById(R.id.etCountry);
        editpincode = (EditText) findViewById(R.id.etZip);
        editadress1 = (EditText) findViewById(R.id.etAddressLine1);
        editadress2 = (EditText) findViewById(R.id.etAddressLine2);
    }


    private void submitAllregistrationValues()
    {
        Header header = new Header() ;
        header.setAction("ADD");
        header.setUserid("NEW");
        header.setPassword("323434");
        header.setPhnumber("9876543654321");

        Details details = new Details() ;
        details.setFirstName(editFirstName.getText().toString());
        details.setLastName(editLastName.getText().toString());
        details.setEmail(editemail.getText().toString());
        details.setDateOfBirth(editdateofbirth.getText().toString());
        details.setDeviceId("asdlfakjsldfjlasdjflk");
        details.setLangId("1");
        details.setUserType("EU");

        //"dateOfBirth":"2015/12/15", "email":";laksjdf;laksjf", "deviceId":"asdlfakjsldfjlasdjflk","langId":"1", "userType":"EU"
        Address address = new Address(editadress1.getText().toString(),editadress2.getText().toString(),editstreet.getText().toString(),
                editCity.getText().toString(),editState.getText().toString(),editCountry.getText().toString(),editpincode.getText().toString());
        List<Address> addresses = new ArrayList<Address>() ;
        addresses.add(address);

        Registration registration = new Registration();

        registration.setHeader(header);
        registration.setDetails(details);
        registration.setAddressLists(addresses);

        System.out.println("JsonUtil String" + JsonUtil.toJSon(registration));

        postRegistration(JsonUtil.toJSon(registration));
    }
}
