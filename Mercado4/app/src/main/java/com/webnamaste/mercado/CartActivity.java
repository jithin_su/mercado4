package com.webnamaste.mercado;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.webnamaste.database.DatabaseHandler;
import com.webnamaste.pojo.CartItem;
import com.webnamaste.pojo.FavouriteItem;
import com.webnamaste.singletons.StoriCart;

import java.util.ArrayList;

/**
 * The Cart Page that displays the cart items
 * and allows user to update cart items.
 *
 * @author Sushin PS
 * @version 1.0
 * @since 2015-06-12
 */


public class CartActivity extends AppCompatActivity {

    ListView lvCart;
    TextView tvNoItems, tvCartTotal;
    LinearLayout buttonCheckOut;
    ArrayList<CartItem> productArrayList;

    RelativeLayout rlContinueShopping;
    LinearLayout llCheckOut;

    private static final String RUPEE = "\u20B9 ";
    DatabaseHandler databaseHandler;

    CartAdapter cartAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        tvCartTotal = (TextView) findViewById(R.id.tvCartTotal);

        databaseHandler = new DatabaseHandler(getApplicationContext());
        productArrayList = databaseHandler.getCartItems();

        updateCartTotal();

        tvNoItems = (TextView) findViewById(R.id.tvNoItems);
        rlContinueShopping = (RelativeLayout) findViewById(R.id.rlContinueShopping);
        rlContinueShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        llCheckOut = (LinearLayout) findViewById(R.id.llCheckOut);
        llCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCheckOut = new Intent(CartActivity.this, CheckOutActivity.class);
                startActivity(intentCheckOut);
            }
        });

        if (productArrayList.size() > 0) {
            lvCart = (ListView) findViewById(R.id.lvCart);
            cartAdapter = new CartAdapter(getApplicationContext(), productArrayList);
            lvCart.setAdapter(cartAdapter);
            lvCart.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(CartActivity.this, ProductDetailsActivity.class);
                    startActivity(intent);
                }
            });
        } else {
            tvNoItems.setVisibility(View.VISIBLE);
        }


        buttonCheckOut = (LinearLayout) findViewById(R.id.llCheckOut);
        buttonCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (databaseHandler.getCartItems().size() > 0) {
                    Intent intent = new Intent(CartActivity.this, CheckOutActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(CartActivity.this,"No items in your cart", Toast.LENGTH_SHORT);
                }
            }
        });
    }

    private void updateCartTotal() {
        double total;
        total = databaseHandler.getCartTotal();
        tvCartTotal.setText(RUPEE + total);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public class CartAdapter extends BaseAdapter {

        ArrayList<CartItem> list;
        Context context;

        public CartAdapter(Context context, ArrayList<CartItem> list) {
            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return list.get(position).getProductId();
        }

        /**
         * Class to hold the view of a single row view
         */
        class ViewHolder {

            ImageView ivThumb;
            TextView tvTitle;
            TextView tvPrice;
            TextView tvQuantity;
            Button buttonAddQuantity, buttonReduceQuantity;
            ImageView ivFav;
            ImageView ivDelete;

            ViewHolder(View v) {
                ivThumb = (ImageView) v.findViewById(R.id.thumb);
                tvTitle = (TextView) v.findViewById(R.id.tvTitle);
                tvPrice = (TextView) v.findViewById(R.id.tvPrice);
                tvQuantity = (TextView) v.findViewById(R.id.tvQuantity);
                buttonAddQuantity = (Button) v.findViewById(R.id.buttonPlus);
                buttonReduceQuantity = (Button) v.findViewById(R.id.buttonMinus);
                ivFav = (ImageView) v.findViewById(R.id.ivFav);
                ivDelete = (ImageView) v.findViewById(R.id.ivDeleteItem);
            }
        }

        /**
         * This overrided method is used to create view for each row of list dynamically
         * of list dynamically
         *
         * @param position    The position of the item within the adapter's data set of the item whose view we want.
         * @param convertView The old view to reuse, if possible. Note: You should check that this view is non-null
         *                    and of an appropriate type before using. If it is not possible to convert this view to
         *                    display the correct data, this method can create a new view. Heterogeneous lists can
         *                    specify their number of view types, so that this View is always of the right type.
         * @param parent      The parent that this view will eventually be attached to.
         * @return View This returns view of a single file.
         */
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View row = convertView;
            ImageView ivThumb;
            TextView tvTitle;
            TextView tvPrice;
            Button buttonAddQuantity, buttonReduceQuantity;
            ImageView ivFav;
            ImageView ivDelete;

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.cart_item, parent, false);

            ivThumb = (ImageView) row.findViewById(R.id.thumb);
            tvTitle = (TextView) row.findViewById(R.id.tvTitle);
            tvPrice = (TextView) row.findViewById(R.id.tvPrice);
            final TextView tvQuantity = (TextView) row.findViewById(R.id.tvQuantity);
            buttonAddQuantity = (Button) row.findViewById(R.id.buttonPlus);
            buttonReduceQuantity = (Button) row.findViewById(R.id.buttonMinus);
            ivFav = (ImageView) row.findViewById(R.id.ivFav);
            ivDelete = (ImageView) row.findViewById(R.id.ivDeleteItem);

            final CartItem temp = list.get(position);
            ivThumb.setImageResource(temp.getImageId());
            tvTitle.setText(temp.getProductName());
            tvPrice.setText(RUPEE + Double.toString(temp.getProductPrice()));
            tvQuantity.setText(Integer.toString(temp.getQuantity()));
            buttonAddQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list.get(position).incrementQuantity();
                    databaseHandler.updateCart(list.get(position));
                    cartAdapter.notifyDataSetChanged();
                    updateCartTotal();
                }
            });

            buttonReduceQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list.get(position).decrementQuantity();
                    databaseHandler.updateCart(list.get(position));
                    cartAdapter.notifyDataSetChanged();
                    updateCartTotal();;
                }
            });


            ivFav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(databaseHandler.addToFavourites(new FavouriteItem(temp.getProductId(), temp.getShopId(), temp.getProductName(), temp.getProductPrice(), R.drawable.sample_product))){
                        Toast.makeText(StoriCart.getAppContext(), "Added to Favourites", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(StoriCart.getAppContext(), "Already in Favourites", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    databaseHandler.deleteFromCart(list.get(position));
                    list.remove(position);
                    cartAdapter.notifyDataSetChanged();
                    Toast.makeText(context, "Item Deleted", Toast.LENGTH_LONG).show();
                    if(list.size() == 0){
                        tvNoItems.setVisibility(View.VISIBLE);
                    }
                    updateCartTotal();
                }
            });
            return row;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        productArrayList = databaseHandler.getCartItems();
        if (productArrayList.size() > 0) {
            lvCart = (ListView) findViewById(R.id.lvCart);
            cartAdapter = new CartAdapter(getApplicationContext(), productArrayList);
            lvCart.setAdapter(cartAdapter);
            lvCart.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(CartActivity.this, ProductDetailsActivity.class);
                    startActivity(intent);
                }
            });
        } else {
            tvNoItems.setVisibility(View.VISIBLE);
        }
    }
}
