package com.webnamaste.mercado;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.webnamaste.database.SessionManager;
import com.webnamaste.pojo.Address;
import com.webnamaste.pojo.Details;
import com.webnamaste.pojo.Header;
import com.webnamaste.pojo.HistoryOrder;
import com.webnamaste.pojo.JsonUtil;
import com.webnamaste.pojo.Registration;
import com.webnamaste.pojo.User;
import com.webnamaste.singletons.StoriCart;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * The Account Data page allows user to manage his account.
 *
 * @author Sushin PS
 * @version 1.0
 * @since 2015-06-12
 */

public class AccountActivity extends AppCompatActivity {

    private Toolbar toolbar;
    SessionManager sessionManager;
    TextView tvWelcome;
    Button buttonSignIn, buttonProfile, buttonAddress, buttonSignOut;

    

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        bindAllViews();

        // To setup toolbar
        setUpToolbar();

        sessionManager = new SessionManager(StoriCart.getAppContext());
        decideView();

        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccountActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        buttonSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.logoutUser();
                finish();
                Intent intent = new Intent(AccountActivity.this, AccountActivity.class);
                startActivity(intent);
            }
        });

        buttonProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccountActivity.this, AccountProfileActivity.class);
                startActivity(intent);
            }
        });

        buttonAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccountActivity.this, AccountAddressActivity.class);
                startActivity(intent);
            }
        });




    }






    private void decideView() {
        if(sessionManager.isLoggedIn()){
            buttonSignIn.setVisibility(View.GONE);
            buttonProfile.setVisibility(View.VISIBLE);
            buttonAddress.setVisibility(View.GONE);
            buttonSignOut.setVisibility(View.VISIBLE);
            setAccount();
        } else {
            buttonSignIn.setVisibility(View.VISIBLE);
            buttonProfile.setVisibility(View.GONE);
            buttonAddress.setVisibility(View.GONE);
            buttonSignOut.setVisibility(View.GONE);
        }
    }


    private void setAccount() {
        user = sessionManager.getUserDetails();
        tvWelcome.setText("Welcome "+user.getFirstName());
    }

    private void bindAllViews() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        tvWelcome = (TextView) findViewById(R.id.tvWelcome);
        buttonSignIn = (Button) findViewById(R.id.buttonSignIn);
        buttonProfile = (Button) findViewById(R.id.buttonProfile);
        buttonAddress = (Button) findViewById(R.id.buttonAddress);
        buttonSignOut = (Button) findViewById(R.id.buttonSignOut);
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        NavigationDrawerFragment drawerFragment;
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
