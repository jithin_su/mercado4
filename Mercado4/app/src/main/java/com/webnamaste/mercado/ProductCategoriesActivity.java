package com.webnamaste.mercado;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.webnamaste.adapters.CategoryAdapter;
import com.webnamaste.database.DatabaseHandler;
import com.webnamaste.database.SessionManager;
import com.webnamaste.pojo.Category;
import com.webnamaste.pojo.OfflineProduct;
import com.webnamaste.pojo.ProductPrice;
import com.webnamaste.pojo.Shop;
import com.webnamaste.pojo.UnitOfMeasurement;
import com.webnamaste.singletons.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * The Product Categories that displays the product categories
 * and allows user to update cart items.
 *
 * @author Sushin PS
 * @version 1.0
 * @since 2015-06-10
 */
public class ProductCategoriesActivity extends AppCompatActivity {

    private Toolbar toolbar;
    GridView gvProductCategories;
    LinearLayout llFav, llDownload;
    ProgressBar progressBar;
    ArrayList<Category> list, offlineCategoryList;
    ArrayList<OfflineProduct> catalogueList;
    DatabaseHandler databaseHandler;
    Shop shop;
    TextView tvTitle;


    int shopId;

    private RequestQueue requestQueue;

    public static final String URL_STORICART_PRODUCTCATEGORIESSEARCH = "http://storicart.com/services/catalog?searchType=ALLCATSONLY&json=1";
    public static final String URL_STORICART_CATALOGUE = "http://storicart.com/services/catalog?json=1&shopId=";

    //http://128.199.92.116/services/catalog?shopId=2&json=1&searchType=ALLCATSONLY

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_categories);

        shopId = getIntent().getIntExtra("shopId", 0);
        databaseHandler = new DatabaseHandler(getApplicationContext());
        shop = databaseHandler.getShop(shopId);

        llFav = (LinearLayout) findViewById(R.id.llFav);
        llDownload = (LinearLayout) findViewById(R.id.buttonDownload);
        llDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Use the Builder class for convenient dialog construction
                final AlertDialog.Builder builder = new AlertDialog.Builder(ProductCategoriesActivity.this);
                builder.setMessage("This will erase previously saved catalogues if any. Do you want to download this catalogue for offline.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                downloadCatalogue();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                builder.create();
                builder.show();

            }
        });
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setText(shop.getShopName());

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        NavigationDrawerFragment drawerFragment;
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);

        populateListView();

    }

    private void downloadCatalogue() {
        gvProductCategories.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        requestQueue = VolleySingleton.getInstance().getRequestQueue();
        Log.e("URL", getCatalogueUrl(shopId));
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, getCatalogueUrl(shopId), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                offlineCategoryList = new ArrayList<Category>();
                catalogueList = new ArrayList<OfflineProduct>();
                JSONArray categoryJSONArray = null;
                try {
                    categoryJSONArray = jsonObject.getJSONObject("catalogues").getJSONArray("catalogue").getJSONObject(0).getJSONObject("categories").getJSONArray("category");
                    for (int i = 0; i < categoryJSONArray.length(); i++) {
                        Category temp = new Category(categoryJSONArray.getJSONObject(i).getInt("category-id"), R.drawable.stop_shop_logo, categoryJSONArray.getJSONObject(i).getString("category-name"));
                        offlineCategoryList.add(temp);
                        JSONArray productsJSONArray = categoryJSONArray.getJSONObject(i).getJSONObject("products").getJSONArray("product");
                        for (int j = 0; j < productsJSONArray.length(); j++) {
                            JSONObject productJSONObject = productsJSONArray.getJSONObject(j);
                            OfflineProduct offlineProduct = new OfflineProduct(productJSONObject.getInt("product-id"), temp.categoryId, shopId, productJSONObject.getString("product-short-name"), productJSONObject.getString("product-long-name"), productJSONObject.getString("product-icon"), productJSONObject.getString("product-image"), productJSONObject.getString("product-description"), new UnitOfMeasurement(productJSONObject.getJSONObject("uom").getString("description"), productJSONObject.getJSONObject("uom").getBoolean("isqttyfloat"), productJSONObject.getJSONObject("uom").getString("value")), new ProductPrice(productJSONObject.getJSONObject("product-price").getDouble("actual"), productJSONObject.getJSONObject("product-price").getString("currency")));
                            Log.e("short name", offlineProduct.getProductShortName());
                            catalogueList.add(offlineProduct);
                        }
                        if (databaseHandler.addOfflineShop(offlineCategoryList, catalogueList)) {
                            Toast.makeText(ProductCategoriesActivity.this, "Download Success", Toast.LENGTH_SHORT).show();
                            gvProductCategories.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.INVISIBLE);
                        } else {
                            Toast.makeText(ProductCategoriesActivity.this, "Download Failed", Toast.LENGTH_SHORT).show();
                            gvProductCategories.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    Toast.makeText(ProductCategoriesActivity.this, "Download Failed", Toast.LENGTH_SHORT).show();
                    gvProductCategories.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                    e.printStackTrace();
                    Log.e("JSON Error", e.toString());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(ProductCategoriesActivity.this, "Download Failed", Toast.LENGTH_SHORT).show();
                gvProductCategories.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
                Log.e("Volley Error", volleyError.toString());
            }
        });
        requestQueue.add(request);
    }

    private String getCatalogueUrl(int shopId) {
        return URL_STORICART_CATALOGUE + shopId;
    }


    private void populateListView() {
        Log.e("URL",getRequestUrl(shopId));
        requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, getRequestUrl(shopId), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    list = new ArrayList<Category>();
                    JSONArray shopsJSONArray = jsonObject.getJSONObject("catalogues").getJSONArray("catalogue").getJSONObject(0).getJSONObject("categories").getJSONArray("category");
                    for (int i = 0; i < shopsJSONArray.length(); i++) {
                        Category temp = new Category(shopsJSONArray.getJSONObject(i).getInt("category-id"), R.drawable.stop_shop_logo, shopsJSONArray.getJSONObject(i).getString("category-name"));
                        list.add(temp);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressBar = (ProgressBar) findViewById(R.id.progressBar);
                progressBar.setVisibility(View.GONE);
                if (list.size() > 0) {
                    gvProductCategories = (GridView) findViewById(R.id.gvProductCategories);
                    gvProductCategories.setAdapter(new CategoryAdapter(getApplicationContext(), list));
                    gvProductCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent shopsActivity = new Intent(ProductCategoriesActivity.this, ProductListActivity.class);
                            shopsActivity.putExtra("shopId", shopId);
                            shopsActivity.putExtra("categoryId", list.get(position).categoryId);
                            startActivity(shopsActivity);
                        }
                    });
                } else {
                    TextView tvNoItems = (TextView) findViewById(R.id.tvNoItems);
                    tvNoItems.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("Error", volleyError.toString());
            }
        });
        requestQueue.add(request);
    }

    private String getRequestUrl(int shopId) {
        return URL_STORICART_PRODUCTCATEGORIESSEARCH + "&shopId=" + shopId;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {
            Intent intentCart = new Intent(ProductCategoriesActivity.this, CartActivity.class);
            startActivity(intentCart);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void openFavourites(View view) {
        Intent intentFavourites = new Intent(this, FavouritesActivity.class);
        startActivity(intentFavourites);
    }
}
