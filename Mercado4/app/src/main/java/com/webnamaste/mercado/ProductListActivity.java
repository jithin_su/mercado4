package com.webnamaste.mercado;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.webnamaste.database.DatabaseHandler;
import com.webnamaste.pojo.CartItem;
import com.webnamaste.pojo.FavouriteItem;
import com.webnamaste.pojo.Product;
import com.webnamaste.pojo.ProductPrice;
import com.webnamaste.pojo.UnitOfMeasurement;
import com.webnamaste.singletons.StoriCart;
import com.webnamaste.singletons.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ProductListActivity extends AppCompatActivity {

    private Toolbar toolbar;
    ListView lvProducts;
    ArrayList<Product> list;
    int shopId, categoryId;
    TextView tvFavCount;

    TextView tvTitle;
    DatabaseHandler databaseHandler;
    TextView tvNoData;

    TextView tvQuantity;
    Button buttonPlus, buttonMinus;

    NetworkImageView ivShopLogo;


    private RequestQueue requestQueue;
    private ImageLoader mImageLoader;

    public static final String URL_STORICART_PRODUCTSSEARCH = "http://storicart.com/services/catalog?json=1&searchType=CATPRODS";

    // http://storicart.com/services/catalog?shopId=2&json=1&searchType=CATPRODS&categoryId=2

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        shopId = getIntent().getIntExtra("shopId", 0);
        categoryId = getIntent().getIntExtra("categoryId", 0);

        databaseHandler = new DatabaseHandler(getApplicationContext());
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        ivShopLogo = (NetworkImageView) findViewById(R.id.ivShopLogo);
        tvNoData = (TextView) findViewById(R.id.tvNoItems);
        tvTitle.setText(databaseHandler.getShop(shopId).getShopName());
        mImageLoader = VolleySingleton.getInstance().getImageLoader();
        ivShopLogo.setImageUrl(databaseHandler.getShop(shopId).getGetImageUrlLarge(), mImageLoader);

        NavigationDrawerFragment drawerFragment;
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);

        populateListView();

    }

    /**
     * Method to get data from API and add to ListView
     *
     */
    private void populateListView() {
        requestQueue = Volley.newRequestQueue(this);
        Log.e("url", getRequestUrl(shopId, categoryId));
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, getRequestUrl(shopId, categoryId), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    JSONArray productsJSONArray = jsonObject.getJSONObject("catalogues").getJSONArray("catalogue").getJSONObject(0).getJSONObject("categories").getJSONArray("category").getJSONObject(0).getJSONObject("products").getJSONArray("product");
                    list = new ArrayList<Product>();
                    for (int i = 0; i < productsJSONArray.length(); i++) {
                        ProductPrice productPrice;
                        JSONObject productJSONObject = productsJSONArray.getJSONObject(i);
                        Log.d("object "+i, productJSONObject.toString());
                        if (productJSONObject.getJSONObject("product-price").has("offer")) {
                            productPrice = new ProductPrice(productJSONObject.getJSONObject("product-price").getDouble("actual"), productJSONObject.getJSONObject("product-price").getDouble("offer"), productJSONObject.getJSONObject("product-price").getString("currency"));
                        } else {
                            productPrice = new ProductPrice(productJSONObject.getJSONObject("product-price").getDouble("actual"), productJSONObject.getJSONObject("product-price").getString("currency"));
                        }
                        Product temp = new Product(productJSONObject.getInt("product-id"), shopId, productJSONObject.getString("product-short-name"), productJSONObject.getString("product-long-name"), productJSONObject.getString("product-icon"), productJSONObject.getString("product-image"), productJSONObject.getString("product-description"), new UnitOfMeasurement(productJSONObject.getJSONObject("uom").getString("description"), productJSONObject.getJSONObject("uom").getBoolean("isqttyfloat"), productJSONObject.getJSONObject("uom").getString("value")), productPrice);
                        list.add(temp);
                    }
                    lvProducts = (ListView) findViewById(R.id.lvProducts);
                    ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
                    progressBar.setVisibility(View.GONE);
                    if (list.size() > 0) {
                        lvProducts.setAdapter(new ProductsAdapter(ProductListActivity.this, list));
                        lvProducts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Intent productDetailsActivity = new Intent(ProductListActivity.this, ProductDetailsActivity.class);
                                Product selected = list.get(position);
                                productDetailsActivity.putExtra("productId", selected.getProductId());
                                productDetailsActivity.putExtra("shopId", selected.getShopId());
                                startActivity(productDetailsActivity);
                            }
                        });
                    } else {
                        tvNoData.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("Error", volleyError.toString());
            }
        });
        requestQueue.add(request);
    }

    private String getRequestUrl(int shopId, int categoryId) {
        return URL_STORICART_PRODUCTSSEARCH + "&shopId=" + shopId + "&categoryId=" + categoryId;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {
            Intent intentCart = new Intent(ProductListActivity.this, CartActivity.class);
            startActivity(intentCart);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class ProductsAdapter extends BaseAdapter {

        ArrayList<Product> list;
        Context context;
        DatabaseHandler databaseHandler;

        private static final String RUPEE = "\u20B9 ";

        public ProductsAdapter(Context context, ArrayList<Product> list) {
            this.context = context;
            this.list = list;
            databaseHandler = new DatabaseHandler(context);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return list.get(position).getProductId();
        }

        /**
         * Class to hold the view of a single row view
         */
        class ViewHolder {

            NetworkImageView ivThumb;
            TextView tvTitle;
            TextView tvPrice;
            TextView tvOffer;
            ImageView ivFav;
            ImageView ivCart;

            ViewHolder(View v) {
                ivThumb = (NetworkImageView) v.findViewById(R.id.thumb);
                tvTitle = (TextView) v.findViewById(R.id.tvTitle);
                tvPrice = (TextView) v.findViewById(R.id.tvPrice);
                tvOffer = (TextView) v.findViewById(R.id.tvOfferPrice);
                ivFav = (ImageView) v.findViewById(R.id.ivFav);
                ivCart = (ImageView) v.findViewById(R.id.ivCart);
            }
        }

        /**
         * This overrided method is used to create view for each row of list dynamically
         * of list dynamically
         *
         * @param position    The position of the item within the adapter's data set of the item whose view we want.
         * @param convertView The old view to reuse, if possible. Note: You should check that this view is non-null
         *                    and of an appropriate type before using. If it is not possible to convert this view to
         *                    display the correct data, this method can create a new view. Heterogeneous lists can
         *                    specify their number of view types, so that this View is always of the right type.
         * @param parent      The parent that this view will eventually be attached to.
         * @return View This returns view of a single file.
         */
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View row = convertView;
            ViewHolder holder = null;

            requestQueue = VolleySingleton.getInstance().getRequestQueue();
            mImageLoader = VolleySingleton.getInstance().getImageLoader();

            if (row == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.products_list_item, parent, false);
                holder = new ViewHolder(row);
                row.setTag(holder);
            } else {
                holder = (ViewHolder) row.getTag();
            }

            final Product temp = list.get(position);
            holder.ivThumb.setImageUrl(temp.getIcon(), mImageLoader);
            holder.tvTitle.setText(temp.getProductShortName());
            holder.tvPrice.setText(temp.getProductPrice().toString());
            if (temp.getProductPrice().hasOffer()) {
                holder.tvPrice.setBackgroundResource(R.drawable.bg_strikethrough);
                holder.tvOffer.setVisibility(View.VISIBLE);
                holder.tvOffer.setText(Double.toString(temp.getProductPrice().getOffer()));
            }
            holder.ivFav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Product selected = list.get(position);
                    if (databaseHandler.addToFavourites(new FavouriteItem(selected.getProductId(), selected.getShopId(), selected.getProductShortName(), selected.getProductPrice().toDouble(), R.drawable.sample_product))) {
                        tvFavCount.setText(databaseHandler.getFavouritesCount() + "");
                        Toast.makeText(context, "Added to Favourites", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(StoriCart.getAppContext(), "Already in Favourites", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            holder.ivCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.product_popup);
                    Button buttonCancel = (Button) dialog.findViewById(R.id.buttonCancel);
                    buttonCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    TextView tvTitle, tvPrice, tvOffer;
                    NetworkImageView ivIcon;
                    tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
                    tvPrice = (TextView) dialog.findViewById(R.id.tvPrice);
                    tvOffer = (TextView) dialog.findViewById(R.id.tvOfferPrice);
                    ivIcon = (NetworkImageView) dialog.findViewById(R.id.ivProduct);

                    tvTitle.setText(temp.getProductShortName());
                    tvPrice.setText(temp.getProductPrice().toString());
                    if (temp.getProductPrice().hasOffer()) {
                        tvPrice.setBackgroundResource(R.drawable.bg_strikethrough);
                        tvOffer.setVisibility(View.VISIBLE);
                        tvOffer.setText(Double.toString(temp.getProductPrice().getOffer()));
                    }
                    ivIcon.setImageUrl(temp.getIcon(), mImageLoader);
                    tvQuantity = (TextView) dialog.findViewById(R.id.tvQuantity);
                    buttonPlus = (Button) dialog.findViewById(R.id.buttonPlus);
                    buttonPlus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int quantity = Integer.parseInt(tvQuantity.getText().toString());
                            quantity++;
                            tvQuantity.setText(Integer.toString(quantity));
                        }
                    });

                    buttonMinus = (Button) dialog.findViewById(R.id.buttonMinus);
                    buttonMinus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int quantity = Integer.parseInt(tvQuantity.getText().toString());
                            if (quantity > 1) {
                                quantity--;
                                tvQuantity.setText(Integer.toString(quantity));
                            }
                        }
                    });
                    Button buttonAddToCart = (Button) dialog.findViewById(R.id.button_add_to_cart);
                    buttonAddToCart.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Product temp = list.get(position);
                            CartItem cartItem = new CartItem(temp.getProductId(), temp.getShopId(), temp.getProductShortName(), temp.getProductPrice().toDouble(), Integer.parseInt(tvQuantity.getText().toString()), R.drawable.sample_product);
                            if (databaseHandler.addToCart(cartItem)) {
                                Toast.makeText(getApplicationContext(), "Added to Cart", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Already in Cart", Toast.LENGTH_SHORT).show();
                            }
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            });
            return row;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvFavCount = (TextView) findViewById(R.id.tvFavCount);
        databaseHandler = new DatabaseHandler(StoriCart.getAppContext());
        tvFavCount.setText(databaseHandler.getFavouritesCount() + "");
    }
}
