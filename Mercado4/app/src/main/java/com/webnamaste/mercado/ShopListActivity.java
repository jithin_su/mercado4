package com.webnamaste.mercado;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.webnamaste.adapters.CategoryAdapter;
import com.webnamaste.database.DatabaseHandler;
import com.webnamaste.database.SessionManager;
import com.webnamaste.pojo.Category;
import com.webnamaste.pojo.LocationData;
import com.webnamaste.pojo.Shop;
import com.webnamaste.singletons.StoriCart;
import com.webnamaste.singletons.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ShopListActivity extends AppCompatActivity {

    private Toolbar toolbar;
    GridView gvShops;
    TextView tvCategoryTitle, tvNoData;
    TextView tvFavCount;

    ArrayList<Shop> list;
    DatabaseHandler databaseHandler;

    private RequestQueue requestQueue;
    private ImageLoader mImageLoader;

    SessionManager sessionManager;

    public static final String URL_STORICART_SHOPSEARCH = "http://storicart.com/services/shopsearch/retailtype?json=1";

//    http://storicart.com/services/shopsearch/retailtype?location=2&retailTypeId=3&json=1

    double latitude = 1.0, longitude = 1.0;
    int retailTypeId = 0;
    String categoryTitle;
    LocationData locationData;
    Boolean isGPS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_list);

        Bundle bundle = getIntent().getExtras();
        isGPS = bundle.getBoolean("isGPS", false);
        if(isGPS){
            locationData = new LocationData(isGPS, bundle.getDouble("latitude", 1.0), longitude = bundle.getDouble("longitude", 1.0), "");
        } else {
            locationData = new LocationData(isGPS, 0, 0, bundle.getString("location_name"));
        }
        retailTypeId = bundle.getInt("categoryId", 0);
        categoryTitle = bundle.getString("categoryTitle", "");

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        NavigationDrawerFragment drawerFragment;
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);

        tvCategoryTitle = (TextView) findViewById(R.id.tvCategoryTitle);
        tvCategoryTitle.setText(categoryTitle.toUpperCase());
        populateListView();


    }

    /**
     * Method to get data from API and add to ListView
     *
     */
    private void populateListView() {
        databaseHandler = new DatabaseHandler(getApplicationContext());
        requestQueue = Volley.newRequestQueue(this);
        sessionManager = new SessionManager(StoriCart.getAppContext());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, getRequestUrl(sessionManager.getUserId(), retailTypeId, locationData), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d("Response", jsonObject.toString());
                try {

                    JSONArray shopsJSONArray = jsonObject.getJSONObject("shopSearchResult").getJSONArray("shops");
                    list = new ArrayList<Shop>();
                    for (int i = 0; i < shopsJSONArray.length(); i++) {
                        Shop temp = new Shop(shopsJSONArray.getJSONObject(i).getInt("shopId"), shopsJSONArray.getJSONObject(i).getString("shopName"), shopsJSONArray.getJSONObject(i).getString("orderSendToPhone"), shopsJSONArray.getJSONObject(i).getDouble("latitude"), shopsJSONArray.getJSONObject(i).getDouble("longitude"), shopsJSONArray.getJSONObject(i).getDouble("minOrderAmount"), shopsJSONArray.getJSONObject(i).getInt("minTimeToDeliver"), /*shopsJSONArray.getJSONObject(i).getString("imgUrlSmall")*/"http://storicart.com/images/products/no_Img.png", /*shopsJSONArray.getJSONObject(i).getString("imgUrlLarge")*/"http://storicart.com/images/products/no_Img.png");
                        list.add(temp);
                        databaseHandler.addToShops(temp);
                    }
                    gvShops = (GridView) findViewById(R.id.gvShops);
                    ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
                    progressBar.setVisibility(View.GONE);
                    if(list.size() > 0){
                        gvShops.setAdapter(new ShopsAdapter(getApplicationContext(), list));
                        gvShops.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Intent shopsActivity = new Intent(ShopListActivity.this, ProductCategoriesActivity.class);
                                shopsActivity.putExtra("shopId",list.get(position).getShopId());
                                startActivity(shopsActivity);
                            }
                        });

                    } else {
                        tvNoData = (TextView) findViewById(R.id.tvNoItems);
                        tvNoData.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSON Error", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);
    }

    private String getRequestUrl(int userId, int retailTypeId, LocationData locationData) {
        if(locationData.isGPS()){
            return URL_STORICART_SHOPSEARCH + "&userId=" + userId + "&retailTypeId=" + retailTypeId + "&latitude=" + locationData.getLatitude() + "&longitutude=" + locationData.getLongitude();
        } else {
            return URL_STORICART_SHOPSEARCH + "&userId=" + userId + "&retailTypeId=" + retailTypeId + "&location=" + locationData.getName();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_shop_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {
            Intent intentCart = new Intent(ShopListActivity.this, CartActivity.class);
            startActivity(intentCart);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class ShopsAdapter extends BaseAdapter {

        ArrayList<Shop> list;
        Context context;

        public ShopsAdapter(Context context, ArrayList<Shop> list){
            this.context=context;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        /**
         * Class to hold the view of a single row view
         *
         */
        class ViewHolder{
            NetworkImageView ivThumb;
            TextView tvTitle;
            ViewHolder(View v){
                ivThumb = (NetworkImageView) v.findViewById(R.id.ivShopLogo);
                tvTitle = (TextView) v.findViewById(R.id.tvCategoryTitle);
            }
        }

        /**
         * This overrided method is used to create view for each row of list dynamically
         * of list dynamically
         *
         * @param position The position of the item within the adapter's data set of the item whose view we want.
         * @param convertView  The old view to reuse, if possible. Note: You should check that this view is non-null
         *                     and of an appropriate type before using. If it is not possible to convert this view to
         *                     display the correct data, this method can create a new view. Heterogeneous lists can
         *                     specify their number of view types, so that this View is always of the right type.
         * @param parent The parent that this view will eventually be attached to.
         * @return View This returns view of a single file.
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View row=convertView;
            ViewHolder holder=null;

            requestQueue = VolleySingleton.getInstance().getRequestQueue();
            mImageLoader = VolleySingleton.getInstance().getImageLoader();

            if(row==null){
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.shop_list_item, parent, false);
                holder = new ViewHolder(row);
                row.setTag(holder);
            } else{
                holder = (ViewHolder) row.getTag();
            }
            Shop temp = list.get(position);
            holder.ivThumb.setImageUrl(temp.getImageUrlSmall(), mImageLoader);
            holder.tvTitle.setText(temp.getShopName());
            return row;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        databaseHandler = new DatabaseHandler(StoriCart.getAppContext());
        tvFavCount = (TextView) findViewById(R.id.tvFavCount);
        tvFavCount.setText(databaseHandler.getFavouritesCount() + "");
    }

    public void openFavourites(View view) {
        Intent intentFavourites = new Intent(this, FavouritesActivity.class);
        startActivity(intentFavourites);
    }
}
