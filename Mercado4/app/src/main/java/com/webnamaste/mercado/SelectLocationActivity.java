package com.webnamaste.mercado;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.webnamaste.customviews.autocompletetextview.MultiAutoCompleteSearchView;
import com.webnamaste.database.DatabaseHandler;
import com.webnamaste.database.SessionManager;
import com.webnamaste.network.ConnectionDetector;
import com.webnamaste.network.GPSTracker;
import com.webnamaste.singletons.StoriCart;
import com.webnamaste.singletons.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * The Select Location page allows user to select
 * location to search shops.
 *
 * @author Sushin PS
 * @version 1.0
 * @since 2015-06-12
 */


public class SelectLocationActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    /**
     * Static Variables.
     */
    protected static final String TAG = "storicart";

    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;


    public List<String> suggest;
    public ArrayAdapter<String> aAdapter;


    /**
     * Represents a geographical location.
     */
    protected Location mLastLocation;

    /**
     * View Components.
     */
    private Toolbar toolbar;
    LinearLayout buttonUseMyLocation;
    AutoCompleteTextView etSearchLocation;
    TextView tvFavCount;
    Menu menu;

    /**
     * For checking internet connectivity.
     */
    ConnectionDetector connectionDetector;

    /**
     * Helps to manage session using SharedPreferences.
     */
    SessionManager sessionManager;

    DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_location);

        // Binds all views to activity
        bindAllViews();

        // To setup toolbar
        setUpToolbar();

        // Build Google API Client to get location data
        buildGoogleApiClient();

        sessionManager = new SessionManager(getApplicationContext());

        suggest = new ArrayList<String>();

        buttonUseMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectionDetector = new ConnectionDetector(getApplicationContext());
                if (connectionDetector.isConnectingToInternet()) {
                    mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    if (mLastLocation != null) {
                        Intent intent;
                        intent = new Intent(SelectLocationActivity.this, ShopCategoriesActivity.class);
                        intent.putExtra("latitude", mLastLocation.getLatitude());
                        intent.putExtra("longitude", mLastLocation.getLongitude());
                        intent.putExtra("isGPS", true);
                        startActivity(intent);
                    } else {
                        Intent intent;
                        intent = new Intent(SelectLocationActivity.this, ShopCategoriesActivity.class);
                        intent.putExtra("latitude", 11.0000111);
                        intent.putExtra("longitude", 11.55515222);
                        intent.putExtra("isGPS", true);
                        startActivity(intent);
                    }

                } else {
                    showAlertDialog(SelectLocationActivity.this, "No Internet Connection",
                            "You don't have internet connection.");
                }
            }
        });

        etSearchLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String newText = s.toString();
                new getJson(0).execute(newText);
            }
        });

        etSearchLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                connectionDetector = new ConnectionDetector(getApplicationContext());
                if (connectionDetector.isConnectingToInternet()) {
                    Intent intent;
                    intent = new Intent(SelectLocationActivity.this, ShopCategoriesActivity.class);
                    intent.putExtra("location_name", mLastLocation.getLatitude());
                    intent.putExtra("isGPS", false);
                    startActivity(intent);
                } else {
                    showAlertDialog(SelectLocationActivity.this, "No Internet Connection",
                            "You don't have internet connection.");
                }
            }
        });
//        }
    }

    /**
     * Method to display setup Toolbar
     */
    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        NavigationDrawerFragment drawerFragment;
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
    }

    /**
     * Method to display bind all views to activity
     */
    private void bindAllViews() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        etSearchLocation = (AutoCompleteTextView) findViewById(R.id.etSearch);
        buttonUseMyLocation = (LinearLayout) findViewById(R.id.llUseMyLocation);
        suggest = new ArrayList<String>();
        etSearchLocation.setThreshold(1);

    }


    /**
     * Method to display simple Alert Dialog
     *
     * @param context - application context
     * @param title   - alert dialog title
     * @param message - alert message
     */
    public void showAlertDialog(Context context, String title, String message) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog));
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu, menu);
        intiSearchText(menu);
        return true;
    }

    MultiAutoCompleteSearchView.SearchAutoComplete searchAutoComplete;


    private void intiSearchText(Menu menu) {

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final MultiAutoCompleteSearchView searchView = (MultiAutoCompleteSearchView)
                MenuItemCompat.getActionView(searchItem);

        searchView.setQueryHint("Type any word");

        searchAutoComplete =
                (MultiAutoCompleteSearchView.SearchAutoComplete) searchView
                        .findViewById(R.id.search_src_text);
//since words are very short
        searchAutoComplete.setThreshold(1);

        searchAutoComplete.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String newText = s.toString();
                new getJson(1).execute(newText);
            }
        });

        searchAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

//        searchAutoComplete.setAdapter(new DelimiterAdapter(
//                this,
//                android.R.layout.simple_dropdown_item_1line
//        ));

//        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                String stringBefore, newString;
//                stringBefore = ((DelimiterAdapter)parent.getAdapter()).getMainString();
//                newString = parent.getAdapter().getItem(position).toString();
//
//                searchView.setQuery(stringBefore+newString, false);
//
//            }
//        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {
            Intent intentCart = new Intent(SelectLocationActivity.this, CartActivity.class);
            startActivity(intentCart);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void openFavourites(View view) {
        Intent intentFavourites = new Intent(this, FavouritesActivity.class);
        startActivity(intentFavourites);
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        // Provides a simple way of getting a device's location and is well suited for
        // applications that do not require a fine-grained location and that do not need location
        // updates. Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {

        } else {

        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }


    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    /**
     * Builds a GoogleApiClient. Uses the addApi() method to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        databaseHandler = new DatabaseHandler(StoriCart.getAppContext());
        tvFavCount = (TextView) findViewById(R.id.tvFavCount);
        tvFavCount.setText(databaseHandler.getFavouritesCount() + "");
    }

    class getJson extends AsyncTask<String, Void, Void> {

        public int whichsearch;

        public getJson(int whichsearch) {
            this.whichsearch = whichsearch;
        }

        @Override
        protected Void doInBackground(String... params) {
            Log.e("Asyc Task", "02Aug2015 start");
            suggest.clear();
            String newText = params[0];
            RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, "http://storicart.com/services/loccomjson?locName=" + newText, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            Log.v("LOG", "02Aug2015 " + jsonObject);
                            try {
                                JSONArray locationsJSONArray = jsonObject.getJSONArray("locations");

                                for (int i = 0; i < locationsJSONArray.length(); i++) {
                                    suggest.add(locationsJSONArray.getJSONObject(i).getString("locName"));
                                    Log.e("Asyc Task", "02Aug2015 location " + locationsJSONArray.getJSONObject(i).getString("locName"));
                                }
                                publishProgress();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                }
            });
            requestQueue.add(request);
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            ArrayAdapter<String> aAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, suggest);
            if (whichsearch == 1) {
                searchAutoComplete.setAdapter(aAdapter);
            } else {
                etSearchLocation.setAdapter(aAdapter);
            }

            aAdapter.notifyDataSetChanged();
            Log.v("LOG", "02Aug2015 " + "onProgressUpdate");
        }
    }
}
