package com.webnamaste.mercado;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.webnamaste.database.DatabaseHandler;
import com.webnamaste.pojo.HistoryOrder;
import com.webnamaste.singletons.StoriCart;


public class OrderSummaryActivity extends AppCompatActivity {

    HistoryOrder historyOrder;
    int orderId;

    DatabaseHandler databaseHandler;

    TextView tvAddress, tvTotal, tvOrderId, tvShopName, tvDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_summary);

        databaseHandler = new DatabaseHandler(StoriCart.getAppContext());

        tvAddress = (TextView) findViewById(R.id.tvAddress);
        tvTotal = (TextView) findViewById(R.id.tvTotal);
        tvOrderId = (TextView) findViewById(R.id.tvOrderId);
        tvShopName = (TextView) findViewById(R.id.tvShopName);
        tvDate = (TextView) findViewById(R.id.tvDate);

        orderId = getIntent().getIntExtra("order_id", 0);

        if(orderId != 0){
           // HistoryOrder historyOrder = databaseHandler.getOrder(orderId);
            tvAddress.setText(getIntent().getStringExtra("shipaddress"));
            tvOrderId.setText(Integer.toString(orderId));
           // tvShopName.setText(historyOrder.getShopId()+"");
            tvDate.setText(getIntent().getStringExtra("orderdate"));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_order_summary, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
