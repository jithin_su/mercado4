///Date updated  02 Aug 2015

package com.webnamaste.mercado;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.webnamaste.database.DatabaseHandler;
import com.webnamaste.network.ConnectionDetector;
import com.webnamaste.pojo.Address;
import com.webnamaste.pojo.Item;
import com.webnamaste.pojo.HistoryOrder;
import com.webnamaste.pojo.ProductPrice;
import com.webnamaste.singletons.StoriCart;
import com.webnamaste.singletons.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import java.io.InputStreamReader;
import java.util.ArrayList;


public class HistoryActivity extends AppCompatActivity {

    private Toolbar toolbar;
    ListView lvHistory;
    ArrayList<HistoryOrder> historyOrderArrayList;
    private RequestQueue requestQueue;
    ConnectionDetector connectionDetector;
    DatabaseHandler databaseHandler;
    ProgressBar progressBar;
    SharedPreferences prefs;

    //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(HistoryActivity.this);

    public static final String URL_STORICART_ORDER_RETRIEVAL = "http://storicart.com/services/ordersbyuser?";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        NavigationDrawerFragment drawerFragment;
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        connectionDetector = new ConnectionDetector(StoriCart.getAppContext());
//        databaseHandler = new DatabaseHandler(StoriCart.getAppContext());
        historyOrderArrayList = new ArrayList<HistoryOrder>();
        if(connectionDetector.isConnectingToInternet()){
           populateListViewFromAPI();
        } else {
            populateOfflineData(getRequestUrl());
           //populateListViewFromDB();
        }

      //  readJsonFromFile();

    }
    private void populateOfflineData(String url)
    {
        final ArrayList<HistoryOrder> results = new ArrayList<HistoryOrder>();

        try {
            JSONObject jsonObject = new JSONObject(getResponse(url));
            JSONArray ordersJSONArray = jsonObject.getJSONArray("order");
            ArrayList<Item> items = new ArrayList<Item>();
            for (int i = 0; i < ordersJSONArray.length(); i++) {
                JSONObject orderHeaderJSONObject = ordersJSONArray.getJSONObject(i).getJSONObject("header");
                JSONArray itemsJSONArray = ordersJSONArray.getJSONObject(i).getJSONObject("items").getJSONArray("item");
                for (int j = 0; j < itemsJSONArray.length(); j++) {
                    JSONObject jsonObjectItem = itemsJSONArray.getJSONObject(j);
                    Item temp = new Item(jsonObjectItem.getInt("productId"), orderHeaderJSONObject.getJSONObject("shop").getInt("shopId"), jsonObjectItem.getString("productShortName"), jsonObjectItem.getString("productLongName"), new ProductPrice(jsonObjectItem.getJSONObject("price").getDouble("actual"), jsonObjectItem.getJSONObject("price").getString("currency")), jsonObjectItem.getJSONObject("status").getString("value"), jsonObjectItem.getString("uom"), jsonObjectItem.getInt("orderItemId"), jsonObjectItem.getInt("quantity"));
                    items.add(temp);
                }
                JSONObject addressJSONObject = orderHeaderJSONObject.getJSONObject("shipToAddress");
                HistoryOrder historyOrder = new HistoryOrder(orderHeaderJSONObject.getInt("orderId"), orderHeaderJSONObject.getJSONObject("shop").getInt("shopId"), orderHeaderJSONObject.getString("orderedDate"), orderHeaderJSONObject.getString("deliveryTime"), orderHeaderJSONObject.getJSONObject("status").getString("value"), new Address(addressJSONObject.getString("adLine1"), addressJSONObject.getString("adLine2"), addressJSONObject.getString("street"), addressJSONObject.getString("city"), addressJSONObject.getString("state"), addressJSONObject.getString("country"), addressJSONObject.getString("zip")),items);
                results.add(historyOrder);

            }
            populateListView(results);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void populateListViewFromDB() {
        historyOrderArrayList = databaseHandler.getHistoryOrders();
        lvHistory = (ListView) findViewById(R.id.lvHistory);
        lvHistory.setAdapter(new HistoryAdapter(HistoryActivity.this, historyOrderArrayList));
        progressBar.setVisibility(View.GONE);
    }


    private void populateListView(ArrayList<HistoryOrder> historyOrderArrayList) {
      //  historyOrderArrayList = databaseHandler.getHistoryOrders();
        lvHistory = (ListView) findViewById(R.id.lvHistory);
        lvHistory.setAdapter(new HistoryAdapter(HistoryActivity.this, historyOrderArrayList));
        progressBar.setVisibility(View.GONE);
    }

    public void setResponse(String response,String url) {
        prefs = PreferenceManager.getDefaultSharedPreferences(HistoryActivity.this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(url, response);
        editor.commit();
    }

    public String getResponse(String url) {
        prefs = PreferenceManager.getDefaultSharedPreferences(HistoryActivity.this);
        return prefs.getString(url, "");
    }

    private void readJsonFromFile()
    {
        String mLine = "";
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("filename.txt.rtf")));

            // do reading, usually loop until end of file reading
            mLine = reader.readLine();
            /// while (mLine != null) {
            //process line
            ////    mLine = reader.readLine();
            // }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        System.out.println(mLine);



    }



    private void populateListViewFromAPI() {
        progressBar.setVisibility(View.VISIBLE);
        requestQueue = Volley.newRequestQueue(this);
        final ArrayList<HistoryOrder> results = new ArrayList<HistoryOrder>();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, getRequestUrl(), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {

                    setResponse(jsonObject.toString(),getRequestUrl());

                    JSONArray ordersJSONArray = jsonObject.getJSONArray("order");

                    ArrayList<Item> items = new ArrayList<Item>();
                    for (int i = 0; i < ordersJSONArray.length(); i++) {
                        JSONObject orderHeaderJSONObject = ordersJSONArray.getJSONObject(i).getJSONObject("header");
                        JSONArray itemsJSONArray = ordersJSONArray.getJSONObject(i).getJSONObject("items").getJSONArray("item");
                        for (int j = 0; j < itemsJSONArray.length(); j++) {
                            JSONObject jsonObjectItem = itemsJSONArray.getJSONObject(j);
                            Item temp = new Item(jsonObjectItem.getInt("productId"), orderHeaderJSONObject.getJSONObject("shop").getInt("shopId"), jsonObjectItem.getString("productShortName"), jsonObjectItem.getString("productLongName"), new ProductPrice(jsonObjectItem.getJSONObject("price").getDouble("actual"), jsonObjectItem.getJSONObject("price").getString("currency")), jsonObjectItem.getJSONObject("status").getString("value"), jsonObjectItem.getString("uom"), jsonObjectItem.getInt("orderItemId"), jsonObjectItem.getInt("quantity"));
                            items.add(temp);
                        }
                        JSONObject addressJSONObject = orderHeaderJSONObject.getJSONObject("shipToAddress");
                        HistoryOrder historyOrder = new HistoryOrder(orderHeaderJSONObject.getInt("orderId"), orderHeaderJSONObject.getJSONObject("shop").getInt("shopId"), orderHeaderJSONObject.getString("orderedDate"), orderHeaderJSONObject.getString("deliveryTime"), orderHeaderJSONObject.getJSONObject("status").getString("value"), new Address(addressJSONObject.getString("adLine1"), addressJSONObject.getString("adLine2"), addressJSONObject.getString("street"), addressJSONObject.getString("city"), addressJSONObject.getString("state"), addressJSONObject.getString("country"), addressJSONObject.getString("zip")),items);
                        results.add(historyOrder);

                    }
                    populateListView(results);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        requestQueue.add(request);
    }


    private String getRequestUrl() {
        return URL_STORICART_ORDER_RETRIEVAL + "json=" + 1 + "&userId=" + 1 + "&password=" + "123456";
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class HistoryAdapter extends BaseAdapter {

        ArrayList<HistoryOrder> list;
        Context context;
        DatabaseHandler databaseHandler;

        private static final String RUPEE = "\u20B9 ";

        public HistoryAdapter(Context context, ArrayList<HistoryOrder> list) {
            this.context = context;
            this.list = list;
            databaseHandler = new DatabaseHandler(context);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return list.get(position).getOrderId();
        }

        /**
         * Class to hold the view of a single row view
         */
        class ViewHolder {

            TextView tvTitle;
            TextView tvTotalCount;
            TextView tvDate;
            TextView tvOrderId;
            Button buttonViewOrder;

            ViewHolder(View v) {
                tvTitle = (TextView) v.findViewById(R.id.tvTitle);
                tvTotalCount = (TextView) v.findViewById(R.id.tvTotalCount);
                tvDate = (TextView) v.findViewById(R.id.tvDate);
                tvOrderId = (TextView) v.findViewById(R.id.tvOrderId);
                buttonViewOrder = (Button) v.findViewById(R.id.buttonViewOrder);
            }
        }

        /**
         * This overrided method is used to create view for each row of list dynamically
         * of list dynamically
         *
         * @param position    The position of the item within the adapter's data set of the item whose view we want.
         * @param convertView The old view to reuse, if possible. Note: You should check that this view is non-null
         *                    and of an appropriate type before using. If it is not possible to convert this view to
         *                    display the correct data, this method can create a new view. Heterogeneous lists can
         *                    specify their number of view types, so that this View is always of the right type.
         * @param parent      The parent that this view will eventually be attached to.
         * @return View This returns view of a single file.
         */
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View row = convertView;
            ViewHolder holder = null;

            requestQueue = VolleySingleton.getInstance().getRequestQueue();

            if (row == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.history_item, parent, false);
                holder = new ViewHolder(row);
                row.setTag(holder);
            } else {
                holder = (ViewHolder) row.getTag();
            }

            final HistoryOrder temp = list.get(position);
            holder.tvTitle.setText(temp.getItems().get(0).getProductShortName());
            holder.tvTotalCount.setText("(Total: " + temp.getItems().size() + " item)");
            holder.tvDate.setText(temp.getOrderedDate());
            holder.tvOrderId.setText(temp.getOrderId()+"");
            holder.buttonViewOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HistoryActivity.this, OrderSummaryActivity.class);
                    intent.putExtra("order_id", temp.getOrderId());
                    intent.putExtra("shipaddress",temp.getShipToAddress().toString());
                    intent.putExtra("orderdate",temp.getOrderedDate().toString());
                    startActivity(intent);
                }
            });
            return row;
        }
    }





}
