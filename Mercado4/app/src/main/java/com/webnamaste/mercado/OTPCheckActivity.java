package com.webnamaste.mercado;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.webnamaste.database.SessionManager;
import com.webnamaste.singletons.StoriCart;
import com.webnamaste.singletons.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;


public class OTPCheckActivity extends Activity {


    public static final String URL_STORICART_OTPCHECK = "http://storicart.com/services/otpvalidate?";

    EditText etOTP;
    Button buttonOK;
    String mobileNumber, password;
    SessionManager sessionManager;

    TextView tvSkip;

    JSONObject status;

    private RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpcheck);

        etOTP = (EditText) findViewById(R.id.etOTP);
        buttonOK = (Button) findViewById(R.id.buttonOK);
        tvSkip = (TextView) findViewById(R.id.tvSkip);

        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String OTP = etOTP.getText().toString();
                checkOTP(OTP);
            }
        });

        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sessionManager.isFirstUse()) {
                    Intent intent = new Intent(getApplicationContext(), SelectLocationActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    finish();
                }
            }
        });
    }

    private void checkOTP(String otp) {
        if (otp.trim().length() > 0){
            sessionManager = new SessionManager(StoriCart.getAppContext());
            mobileNumber = sessionManager.getMobileNumber();
            password = sessionManager.getPassword();
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, getRequestUrl(mobileNumber, password, otp), null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObject) {
                    try {
                        Log.e("Respone OTP Check: ",jsonObject.toString());
                        status = jsonObject.getJSONObject("status");
                        if(status.getBoolean("success")){
                            sessionManager.setLoginIn(status.getInt("userId"));
                            if(sessionManager.isFirstUse()) {
                                Intent intent = new Intent(getApplicationContext(), SelectLocationActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                finish();
                            }
                        } else {
                            Toast.makeText(StoriCart.getAppContext(), "Incorrect OTP", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                }
            });
            requestQueue.add(request);
        } else {
            try {
                Toast.makeText(StoriCart.getAppContext(), status.getString("message"), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(StoriCart.getAppContext(), "Incorrect OTP", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getRequestUrl(String mobileNumber, String password, String otp) {
        return URL_STORICART_OTPCHECK + "phone=" + mobileNumber + "&password=" + password + "&otp=" + otp;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_otpcheck, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
    }
}
