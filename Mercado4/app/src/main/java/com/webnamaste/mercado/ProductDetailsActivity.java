package com.webnamaste.mercado;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.webnamaste.adapters.ProductsAdapter;
import com.webnamaste.database.DatabaseHandler;
import com.webnamaste.pojo.CartItem;
import com.webnamaste.pojo.FavouriteItem;
import com.webnamaste.pojo.Product;
import com.webnamaste.pojo.Product2;
import com.webnamaste.pojo.ProductPrice;
import com.webnamaste.pojo.UnitOfMeasurement;
import com.webnamaste.singletons.StoriCart;
import com.webnamaste.singletons.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ProductDetailsActivity extends AppCompatActivity {

    private static final String RUPEE = "\u20B9 ";

    private Toolbar toolbar;
    Button addToCart, buyNow;
    ImageView ivFav;
    LinearLayout llDetails;
    ProgressBar progressBar;
    TextView tvFavCount;

    int productId, shopId;
    Product productObject;
    Product2 temp;

    TextView tvTitle, tvShopName, tvDescription, tvQuantity, tvPrice, tvOffer;
    Button buttonAdd, buttonMinus;

    private RequestQueue requestQueue;
    private ImageLoader mImageLoader;
    NetworkImageView productImage;

    DatabaseHandler databaseHandler;

    public static final String URL_STORICART_PRODUCT_DETAILS = "http://storicart.com/services/catalog?json=1&searchType=PRODDETAIL";

    // http://128.199.92.116/services/catalog?shopId=2&json=1&searchType=PRODDETAIL&productId=2

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        productId = getIntent().getIntExtra("productId",0);
        shopId = getIntent().getIntExtra("shopId",0);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        NavigationDrawerFragment drawerFragment;
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);


        bindAll();
        llDetails.setVisibility(View.INVISIBLE);

        databaseHandler = new DatabaseHandler(getApplicationContext());
        getProductDetails();

    }

    private void bindAll() {

        addToCart = (Button) findViewById(R.id.button_add_to_cart);
        ivFav = (ImageView) findViewById(R.id.ivFav);
        buyNow = (Button) findViewById(R.id.button_buy_now);
        llDetails = (LinearLayout) findViewById(R.id.llDetails);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvShopName = (TextView) findViewById(R.id.tvShopName);
        tvPrice = (TextView) findViewById(R.id.tvPrice);
        tvOffer = (TextView) findViewById(R.id.tvOfferPrice);

        tvDescription = (TextView) findViewById(R.id.tvDescription);
        tvQuantity = (TextView) findViewById(R.id.tvQuantity);
        buttonAdd = (Button) findViewById(R.id.buttonAdd);
        buttonMinus = (Button) findViewById(R.id.buttonMinus);
        productImage = (NetworkImageView) findViewById(R.id.ivProductImage);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {
            Intent intentCart = new Intent(ProductDetailsActivity.this, CartActivity.class);
            startActivity(intentCart);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getProductDetails() {
        requestQueue = VolleySingleton.getInstance().getRequestQueue();
        mImageLoader = VolleySingleton.getInstance().getImageLoader();
        Log.d("url", getRequestUrl(shopId, productId));
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, getRequestUrl(shopId, productId), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    JSONObject product = jsonObject.getJSONObject("product");
                    ProductPrice productPrice;
                    if (product.getJSONObject("product-price").has("offer")) {
                        productPrice = new ProductPrice(product.getJSONObject("product-price").getDouble("actual"), product.getJSONObject("product-price").getDouble("offer"), product.getJSONObject("product-price").getString("currency"));
                    } else {
                        productPrice = new ProductPrice(product.getJSONObject("product-price").getDouble("actual"), product.getJSONObject("product-price").getString("currency"));
                    }
                    productObject = new Product(product.getInt("product-id"), shopId, product.getString("product-short-name"), product.getString("product-long-name"), product.getString("product-icon"), product.getString("product-image"), product.getString("product-description"), new UnitOfMeasurement(product.getJSONObject("uom").getString("description"), product.getJSONObject("uom").getBoolean("isqttyfloat"), product.getJSONObject("uom").getString("value")), productPrice);
                    tvTitle.setText(productObject.getProductLongName());
                    tvDescription.setText(productObject.getDescription());
                    tvShopName.setText(databaseHandler.getShop(shopId).getShopName());
                    tvPrice.setText(productObject.getProductPrice().toString());
                    if (productObject.getProductPrice().hasOffer()) {
                        tvPrice.setBackgroundResource(R.drawable.bg_strikethrough);
                        tvOffer.setVisibility(View.VISIBLE);
                        tvOffer.setText(Double.toString(productObject.getProductPrice().getOffer()));
                    }
                    productImage.setImageUrl(productObject.getImage(), mImageLoader);
                    buttonAdd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int quantity = Integer.parseInt(tvQuantity.getText().toString());
                            quantity++;
                            tvQuantity.setText(Integer.toString(quantity));
                        }
                    });
                    buttonMinus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int quantity = Integer.parseInt(tvQuantity.getText().toString());
                            if(quantity >1) {
                                quantity--;
                                tvQuantity.setText(Integer.toString(quantity));
                            }
                        }
                    });
                    addToCart.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            CartItem cartItem = new CartItem(productObject.getProductId(), productObject.getShopId(), productObject.getProductShortName(), productObject.getProductPrice().toDouble(), Integer.parseInt(tvQuantity.getText().toString()), R.drawable.sample_product);
                            if(databaseHandler.addToCart(cartItem)) {
                                Toast.makeText(getApplicationContext(), "Added to Cart", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Already in Cart", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    buyNow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            CartItem cartItem = new CartItem(productObject.getProductId(), productObject.getShopId(), productObject.getProductShortName(), productObject.getProductPrice().toDouble(), Integer.parseInt(tvQuantity.getText().toString()), R.drawable.sample_product);
                            databaseHandler.addToCart(cartItem);
                            Intent intent = new Intent(ProductDetailsActivity.this, CartActivity.class);
                            startActivity(intent);
                        }
                    });
                    ivFav.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            databaseHandler = new DatabaseHandler(StoriCart.getAppContext());
                            if(databaseHandler.addToFavourites(new FavouriteItem(productObject.getProductId(), productObject.getShopId(), productObject.getProductShortName(), productObject.getProductPrice().toDouble(), R.drawable.sample_product))){
                                tvFavCount.setText(databaseHandler.getFavouritesCount() + "");
                                Toast.makeText(StoriCart.getAppContext(), "Added to Favourites", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(StoriCart.getAppContext(), "Already in Favourites", Toast.LENGTH_SHORT).show();
                            }

                        }
                    });
                    progressBar.setVisibility(View.GONE);
                    llDetails.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("Error", volleyError.toString());
            }
        });
        requestQueue.add(request);
    }

    private String getRequestUrl(int shopId, int productId) {
        return URL_STORICART_PRODUCT_DETAILS + "&shopId=" + shopId + "&productId=" + productId;
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvFavCount = (TextView) findViewById(R.id.tvFavCount);
        databaseHandler = new DatabaseHandler(StoriCart.getAppContext());
        tvFavCount.setText(databaseHandler.getFavouritesCount() + "");
    }

    public void openFavourites(View view) {
        Intent intentFavourites = new Intent(this, FavouritesActivity.class);
        startActivity(intentFavourites);
    }

}
