package com.webnamaste.mercado;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.webnamaste.database.SessionManager;
import com.webnamaste.singletons.StoriCart;
import com.webnamaste.singletons.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;


public class RegisterActivity extends AppCompatActivity {

    public static final String URL_STORICART_OTPGEN = "http://storicart.com/services/otpgen?";

    EditText etMobileNumber;
    String mobileNumber, password;
    TextView tvSkip;
    Button btOK;
    SessionManager sessionManager;

    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        sessionManager = new SessionManager(StoriCart.getAppContext());
        bindAll();
        btOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateOTP();
            }
        });

        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sessionManager.isFirstUse()) {
                    sessionManager.markFirstUse();
                    Intent intent = new Intent(getApplicationContext(), SelectLocationActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    finish();
                }
            }
        });
    }

    private void bindAll() {
        etMobileNumber = (EditText) findViewById(R.id.etMobileNumber);
        btOK = (Button) findViewById(R.id.buttonOK);
        tvSkip = (TextView) findViewById(R.id.tvSkip);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void generateOTP(){
        Log.e("StoriCart", "generateOTP");
        mobileNumber = etMobileNumber.getText().toString();
        password = generatePassword();
        requestQueue = VolleySingleton.getInstance().getRequestQueue();
        if (mobileNumber.trim().length() > 0){
            btOK.setClickable(false);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, getRequestUrl(mobileNumber, password), null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObject) {
                    try {
                        Log.e("StoriCart", "generateOTP - Response: "+jsonObject.toString());
                        JSONObject status = jsonObject.getJSONObject("status");
                        if(status.getBoolean("success")){
                            sessionManager.createMobileAndPassword(mobileNumber, password);
                            Intent intentOTPCheck = new Intent(RegisterActivity.this, OTPCheckActivity.class);
                            startActivity(intentOTPCheck);
                        } else {
                            Toast.makeText(StoriCart.getAppContext(), "Check your connection", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        btOK.setClickable(true);
                        Log.e("JSON Error", e.toString());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    btOK.setClickable(true);
                    Toast.makeText(StoriCart.getAppContext(), "Check your connection and try again", Toast.LENGTH_SHORT).show();
                }
            });
            requestQueue.add(request);
        } else {
            Toast.makeText(RegisterActivity.this, "Enter mobile number", Toast.LENGTH_SHORT).show();
        }
    }

    private String generatePassword() {
        Random random = new Random();
        int num = random.nextInt(9000000) + 1000000;
        return Integer.toString(num);
    }

    private String getRequestUrl(String mobileNumber, String password) {
        return URL_STORICART_OTPGEN + "phone=" + mobileNumber + "&password=" + password + "&source=EUAPP&userId=NEW";
    }
}
