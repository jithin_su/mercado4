package com.webnamaste.mercado;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.webnamaste.database.DatabaseHandler;
import com.webnamaste.pojo.FavouriteItem;
import com.webnamaste.singletons.StoriCart;

import java.util.ArrayList;


public class FavouritesActivity extends AppCompatActivity {

    private Toolbar toolbar;
    ListView lvProducts;
    ArrayList<FavouriteItem> favouritesArrayList;
    TextView tvFavouritesTitle, tvNoItems;
    DatabaseHandler databaseHandler;
    int shopId=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        NavigationDrawerFragment drawerFragment;
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);

        databaseHandler = new DatabaseHandler(StoriCart.getAppContext());

        tvFavouritesTitle = (TextView) findViewById(R.id.tvFavouritesTitle);
        tvNoItems = (TextView) findViewById(R.id.tvNoItems);

        updateFavouritesCount();

        favouritesArrayList = databaseHandler.getFavouriteItems();
        if(favouritesArrayList.size() > 0){
            lvProducts = (ListView) findViewById(R.id.lvFavourites);
            lvProducts.setAdapter(new FavouritesAdapter(favouritesArrayList, FavouritesActivity.this));
            lvProducts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(FavouritesActivity.this, ProductDetailsActivity.class);
                    startActivity(intent);
                }
            });
        } else {
            tvNoItems.setVisibility(View.VISIBLE);
        }

    }

    private void updateFavouritesCount() {;
        tvFavouritesTitle.setText("Favourites (" + databaseHandler.getFavouritesCount() + " items)");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_favourites, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class FavouritesAdapter extends BaseAdapter {

        ArrayList<FavouriteItem> list;
        Context context;

        public FavouritesAdapter(ArrayList<FavouriteItem> list, Context context) {
            this.list = list;
            this.context = context;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return list.get(position).getProductId();
        }

        /**
         * Class to hold the view of a single row view
         *
         */
        class ViewHolder{

            ImageView ivThumb;
            TextView tvTitle;
            TextView tvPrice;
            ImageView ivDelete;
            ImageView ivCart;

            ViewHolder(View v){
                ivThumb = (ImageView) v.findViewById(R.id.thumb);
                tvTitle = (TextView) v.findViewById(R.id.tvTitle);
                tvPrice = (TextView) v.findViewById(R.id.tvPrice);
                ivDelete = (ImageView) v.findViewById(R.id.ivDelete);
                ivCart = (ImageView) v.findViewById(R.id.ivCart);
            }
        }

        /**
         * This overrided method is used to create view for each row of list dynamically
         * of list dynamically
         *
         * @param position The position of the item within the adapter's data set of the item whose view we want.
         * @param convertView  The old view to reuse, if possible. Note: You should check that this view is non-null
         *                     and of an appropriate type before using. If it is not possible to convert this view to
         *                     display the correct data, this method can create a new view. Heterogeneous lists can
         *                     specify their number of view types, so that this View is always of the right type.
         * @param parent The parent that this view will eventually be attached to.
         * @return View This returns view of a single file.
         */

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row=convertView;
            ViewHolder holder=null;

            if(row==null){
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.favourites_list_item,parent,false);
                holder = new ViewHolder(row);
                row.setTag(holder);
            } else{
                holder = (ViewHolder) row.getTag();
            }

            final FavouriteItem temp = list.get(position);
            holder.ivThumb.setImageResource(R.drawable.sample_product);
            holder.tvTitle.setText(temp.getProductName());
            holder.tvPrice.setText(temp.getProductPrice() + "");
            holder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    databaseHandler.deleteFromFavourites(temp);
                    list.remove(position);
                    updateFavouritesCount();
                    notifyDataSetChanged();
                    if (list.size() == 0) {
                        tvNoItems.setVisibility(View.VISIBLE);
                    }
                    Toast.makeText(context, "Deleted from Favourites", Toast.LENGTH_LONG).show();
                }
            });
            holder.ivCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.product_popup);
                    dialog.show();
                }
            });
            return row;
        }
    }
}
