package com.webnamaste.mercado;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.webnamaste.adapters.CategoryAdapter;
import com.webnamaste.customviews.autocompletetextview.MultiAutoCompleteSearchView;
import com.webnamaste.database.DatabaseHandler;
import com.webnamaste.database.SessionManager;
import com.webnamaste.pojo.Category;
import com.webnamaste.pojo.LocationData;
import com.webnamaste.singletons.StoriCart;
import com.webnamaste.singletons.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ShopCategoriesActivity extends AppCompatActivity {

    /**
     * Static Variables.
     */
    protected static final String TAG = "storicart";
    public static final String URL_STORICART_SHOPSEARCH_GPS = "http://storicart.com/services/shopsearch?json=1";
    public static final String URL_STORICART_SHOPSEARCH_NAME = "http://storicart.com/services/shopsearch?json=1";

//    http://storicart.com/services/shopsearch?json=1&userId=1&location=1

    /**
     * View Components.
     */
    private Toolbar toolbar;
    Button buttonChangeLocation;
    TextView tvNoData;
    GridView gvShopCategories;
    ProgressBar progressBar;
    TextView tvFavCount;
    Menu menu;
    public List<String> suggest;

    DatabaseHandler databaseHandler;
    SessionManager sessionManager;


    /**
     * ArrayList to store all shop categories.
     */
    ArrayList<Category> list;

    /**
     * Request queue to handle Volley network operations.
     */
    private RequestQueue requestQueue;
    JsonObjectRequest request;

    /**
     * Variables to store location data.
     */
    LocationData locationData;
    double latitude,longitude;
    String locationName;
    Boolean isGPS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_categories);

        isGPS = getIntent().getBooleanExtra("isGPS", false);

        if(isGPS){
            latitude = getIntent().getDoubleExtra("latitude", 0);
            longitude = getIntent().getDoubleExtra("longitude", 0);
            locationData = new LocationData(isGPS, latitude, longitude, "");
        } else {
            locationName = getIntent().getStringExtra("location_name");
            locationData = new LocationData(isGPS, 0, 0, locationName);
        }

        sessionManager = new SessionManager(StoriCart.getAppContext());
        sessionManager.setLocation(locationData);

        suggest = new ArrayList<String>();
        // Bind all views to Activity
        bindAllViews();

        // To setup toolbar
        setUpToolbar();

        // Get and display shop categories from API
        populateListView();
    }

    /**
     * Method to display bind all views to activity
     *
     */
    private void bindAllViews() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        gvShopCategories = (GridView) findViewById(R.id.gvShopCategories);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        tvNoData = (TextView) findViewById(R.id.tvNoItems);
        buttonChangeLocation = (Button) findViewById(R.id.buttonChangeLocation);

    }

    /**
     * Method to display setup Toolbar
     *
     */
    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        NavigationDrawerFragment drawerFragment;
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
    }

    /**
     * Method to get and display shop categories from API
     *
     */
    private void populateListView() {
        requestQueue = Volley.newRequestQueue(StoriCart.getAppContext());
        sessionManager = new SessionManager(StoriCart.getAppContext());
        request = new JsonObjectRequest(Request.Method.GET, getRequestUrl(sessionManager.getUserId(), locationData), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    JSONArray retailTypesJSONArray = jsonObject.getJSONObject("shopSearchResult").getJSONObject("retailTypes").getJSONArray("retailType");
                    list = new ArrayList<Category>();
                    for (int i = 0; i < retailTypesJSONArray.length(); i++) {
                        JSONObject retailType = retailTypesJSONArray.getJSONObject(i);
                        int imageId = getImageId(retailType.getInt("retailTypeId"));
                        Category temp = new Category(retailType.getInt("retailTypeId"), imageId, retailType.getString("name"));
                        list.add(temp);
                    }
                    progressBar.setVisibility(View.GONE);
                    if(list.size() > 0){
                        sessionManager.setLocation(new LocationData(true, latitude, longitude, ""));
                        gvShopCategories.setAdapter(new CategoryAdapter(ShopCategoriesActivity.this, list));
                        gvShopCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Intent shopsActivity = new Intent(ShopCategoriesActivity.this, ShopListActivity.class);
                                if(isGPS){
                                    shopsActivity.putExtra("isGPS",isGPS);
                                    shopsActivity.putExtra("latitude",latitude);
                                    shopsActivity.putExtra("longitude",longitude);
                                    shopsActivity.putExtra("categoryId",list.get(position).categoryId);
                                    shopsActivity.putExtra("categoryTitle",list.get(position).title);
                                    startActivity(shopsActivity);
                                } else {
                                    shopsActivity.putExtra("isGPS",isGPS);
                                    shopsActivity.putExtra("location_name",locationData.getName());
                                    shopsActivity.putExtra("categoryId",list.get(position).categoryId);
                                    shopsActivity.putExtra("categoryTitle",list.get(position).title);
                                    startActivity(shopsActivity);
                                }

                            }
                        });
                    } else {
                        tvNoData.setVisibility(View.VISIBLE);
                        buttonChangeLocation.setVisibility(View.VISIBLE);
                        buttonChangeLocation.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(ShopCategoriesActivity.this, SelectLocationActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("StoriCart", volleyError.toString());
                requestQueue.add(request);
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }

    private int getImageId(int retailTypeId) {
        switch (retailTypeId) {
            case 1:
                return R.drawable.supermarket;

            case 2:
                return R.drawable.vegitables;

            case 3:
                return R.drawable.fruits;

            case 4:
                return R.drawable.groceries;

            case 5:
                return R.drawable.restaurants;

            case 6:
                return R.drawable.kidsshop;

            case 7:
                return R.drawable.bakers;

            case 8:
                return R.drawable.services;

            case 9:
                return R.drawable.others;

            case 10:
                return R.drawable.stationary;

            case 11:
                return R.drawable.lotions;

            case 12:
                return R.drawable.textiles;

            case 13:
                return R.drawable.others;

            case 14:
                return R.drawable.giftshop;

            default:
                return R.drawable.fruits_icon;
        }
    }

    private String getRequestUrl(int userId, LocationData locationData) {
        if(locationData.isGPS()) {
            return URL_STORICART_SHOPSEARCH_GPS + "&userId=" + userId + "&latitude=" + locationData.getLatitude() + "&longitutude=" + locationData.getLongitude();
        } else {
            return URL_STORICART_SHOPSEARCH_NAME + "&userId=" + userId + "&location=" + locationData.getName();
        }
    }

/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_shop_categories, menu);
        return true;
    }
*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {
            Intent intentCart = new Intent(ShopCategoriesActivity.this, CartActivity.class);
            startActivity(intentCart);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvFavCount = (TextView) findViewById(R.id.tvFavCount);
        databaseHandler = new DatabaseHandler(StoriCart.getAppContext());
        tvFavCount.setText(databaseHandler.getFavouritesCount() + "");
    }

    public void openFavourites(View view) {
        Intent intentFavourites = new Intent(this, FavouritesActivity.class);
        startActivity(intentFavourites);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu, menu);
        intiSearchText(menu);
        return true;
    }

    MultiAutoCompleteSearchView.SearchAutoComplete searchAutoComplete;


    private void intiSearchText(Menu menu){

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final MultiAutoCompleteSearchView searchView = (MultiAutoCompleteSearchView)
                MenuItemCompat.getActionView(searchItem);

        searchView.setQueryHint("Type any word");

        searchAutoComplete =
                (MultiAutoCompleteSearchView.SearchAutoComplete)searchView
                        .findViewById(R.id.search_src_text);
//since words are very short
        searchAutoComplete.setThreshold(1);

        searchAutoComplete.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String newText = s.toString();
                new getJson().execute(newText);
            }
        });

//        searchAutoComplete.setAdapter(new DelimiterAdapter(
//                this,
//                android.R.layout.simple_dropdown_item_1line
//        ));

//        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                String stringBefore, newString;
//                stringBefore = ((DelimiterAdapter)parent.getAdapter()).getMainString();
//                newString = parent.getAdapter().getItem(position).toString();
//
//                searchView.setQuery(stringBefore+newString, false);
//
//            }
//        });
    }

    class getJson extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            Log.e("Asyc Task", "02Aug2015 start");
            suggest.clear();
            String newText = params[0];
            RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, "http://storicart.com/services/prdcomjson?shopId=2&prdName=" + newText, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            Log.v("LOG", "02Aug2015 " + jsonObject);
                            try {
                                JSONArray locationsJSONArray = jsonObject.getJSONArray("products");

                                for (int i = 0; i < locationsJSONArray.length(); i++) {
                                    suggest.add(locationsJSONArray.getJSONObject(i).getString("prodName"));
                                    Log.e("Asyc Task", "02Aug2015 location " + locationsJSONArray.getJSONObject(i).getString("prodName"));
                                }
                                publishProgress();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                }
            });
            requestQueue.add(request);
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            ArrayAdapter<String> aAdapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1, suggest);
            searchAutoComplete.setAdapter(aAdapter);
            aAdapter.notifyDataSetChanged();
            Log.v("LOG", "02Aug2015 " + "onProgressUpdate");
        }
    }
}
