package com.webnamaste.mercado;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.webnamaste.database.SessionManager;
import com.webnamaste.pojo.Address;
import com.webnamaste.pojo.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.security.Security;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.security.cert.CertificateException;
import javax.security.cert.X509Certificate;


public class LoginActivity extends AppCompatActivity {

    private static final String URL_LOGIN = "http://storicart.com/services/applogon?";

    Button buttonRegister, buttonSignIn;
    EditText etUserName, etPassword;
    TextView tvSkip;

    SessionManager session;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        etUserName = (EditText) findViewById(R.id.etUserName);
        etPassword = (EditText) findViewById(R.id.etPassword);
        tvSkip = (TextView) findViewById(R.id.tvSkip);

        session = new SessionManager(getApplicationContext());

        buttonRegister = (Button) findViewById(R.id.buttonCreateAccount);
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.markFirstUse();
                finish();
            }
        });

        buttonSignIn = (Button) findViewById(R.id.buttonSignIn);
        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String username = etUserName.getText().toString();
                final String password = etPassword.getText().toString();

                if (username.trim().length() > 0 && password.trim().length() > 0) {
                    requestQueue = Volley.newRequestQueue(LoginActivity.this);
                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, getRequestUrl(username, password), null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            Log.e("response", jsonObject.toString());
                            try {
                                if (jsonObject.getJSONObject("header").getString("userId").equals("ERRORED: wrong userid/password")) {
                                    Toast.makeText(LoginActivity.this, "Login Failed", Toast.LENGTH_SHORT).show();
                                } else {
                                    JSONArray addressJSONArray = jsonObject.getJSONArray("address");
                                    JSONObject addressJSONObject = addressJSONArray.getJSONObject(0);
                                    Address address = new Address(addressJSONObject.getString("adLine1"), addressJSONObject.getString("adLine2"), addressJSONObject.getString("street"), addressJSONObject.getString("city"), addressJSONObject.getString("state"), addressJSONObject.getString("country"), addressJSONObject.getString("zip"));
                                    JSONObject userJSONObject = jsonObject.getJSONObject("details");
                                    User user = new User(jsonObject.getJSONObject("header").getInt("userId"), userJSONObject.getString("firstName"), userJSONObject.getString("lastName"), userJSONObject.getString("dateOfBirth"), userJSONObject.getString("phone"), userJSONObject.getString("email"), userJSONObject.getString("logonId"), password, address);
                                    session.createLoginSession(user);
                                    session.markFirstUse();
                                    Toast.makeText(LoginActivity.this, "Logged In", Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(LoginActivity.this, "Login Failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.e("Volley Error", volleyError.toString());
                        }
                    });
                    requestQueue.add(request);
                } else {
                    Toast.makeText(LoginActivity.this, "Login Failed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_skip) {
            session.markFirstUse();
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private String getRequestUrl(String username, String password) {
        return URL_LOGIN + "&logonId=" + username + "&password=" + password;
    }

}
