package com.webnamaste.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.webnamaste.mercado.R;
import com.webnamaste.pojo.Category;

import java.util.ArrayList;

/**
 * Adapter to display shop categories, shops and
 * product categories
 *
 * @author Sushin PS
 * @version 1.0
 * @since 2015-06-09
 */

public class CategoryAdapter extends BaseAdapter {

    ArrayList<Category> list;
    Context context;

    public CategoryAdapter(Context context, ArrayList<Category> list){
        this.context=context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Class to hold the view of a single row view
     *
     */
    class ViewHolder{
        ImageView ivThumb;
        TextView tvTitle;
        ViewHolder(View v){
            ivThumb = (ImageView) v.findViewById(R.id.ivCategoryThumb);
            tvTitle = (TextView) v.findViewById(R.id.tvCategoryTitle);
        }
    }

    /**
     * This overrided method is used to create view for each row of list dynamically
     * of list dynamically
     *
     * @param position The position of the item within the adapter's data set of the item whose view we want.
     * @param convertView  The old view to reuse, if possible. Note: You should check that this view is non-null
     *                     and of an appropriate type before using. If it is not possible to convert this view to
     *                     display the correct data, this method can create a new view. Heterogeneous lists can
     *                     specify their number of view types, so that this View is always of the right type.
     * @param parent The parent that this view will eventually be attached to.
     * @return View This returns view of a single file.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row=convertView;
        ViewHolder holder=null;
        if(row==null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.category_list_item,parent,false);
            holder = new ViewHolder(row);
            row.setTag(holder);
        } else{
            holder = (ViewHolder) row.getTag();
        }
        Category temp = list.get(position);
        holder.ivThumb.setImageResource(temp.imageId);
        holder.tvTitle.setText(temp.title);
        return row;
    }
}
