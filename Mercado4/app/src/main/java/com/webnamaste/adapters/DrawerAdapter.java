package com.webnamaste.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.webnamaste.mercado.R;
import com.webnamaste.pojo.DrawerItem;

import java.util.ArrayList;

/**
 * Adapter to display navigation drawer
 *
 * @author Sushin PS
 * @version 1.0
 * @since 2015-06-09
 */
public class DrawerAdapter extends BaseAdapter {

    Context context;
    ArrayList<DrawerItem> list;

    public DrawerAdapter(Context context, ArrayList<DrawerItem> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    /**
     * Class to hold the view of a single row view
     *
     */
    class ViewHolder{

        ImageView ivIcon;
        TextView tvTitle;

        ViewHolder(View v){
            ivIcon = (ImageView) v.findViewById(R.id.imageViewIcon);
            tvTitle = (TextView) v.findViewById(R.id.drawerItemTitle);
        }
    }

    /**
     * This overrided method is used to create view for each row of list dynamically
     * of list dynamically
     *
     * @param position The position of the item within the adapter's data set of the item whose view we want.
     * @param convertView  The old view to reuse, if possible. Note: You should check that this view is non-null
     *                     and of an appropriate type before using. If it is not possible to convert this view to
     *                     display the correct data, this method can create a new view. Heterogeneous lists can
     *                     specify their number of view types, so that this View is always of the right type.
     * @param parent The parent that this view will eventually be attached to.
     * @return View This returns view of a single file.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row=convertView;
        ViewHolder holder=null;

        if(row==null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.drawer_list_item,parent,false);
            holder = new ViewHolder(row);
            row.setTag(holder);
        } else{
            holder = (ViewHolder) row.getTag();
        }

        DrawerItem temp = list.get(position);
        holder.ivIcon.setImageResource(temp.getImageId());
        holder.tvTitle.setText(temp.getTitle());

        return row;
    }

}
