package com.webnamaste.adapters;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.webnamaste.database.DatabaseHandler;
import com.webnamaste.mercado.R;
import com.webnamaste.pojo.Category;
import com.webnamaste.pojo.FavouriteItem;
import com.webnamaste.pojo.Product;
import com.webnamaste.pojo.Product2;
import com.webnamaste.singletons.StoriCart;
import com.webnamaste.singletons.VolleySingleton;

import java.util.ArrayList;

import static com.webnamaste.mercado.R.color.primaryColor;

/**
 * Created by sushinps on 06/06/15.
 */
public class ProductsAdapter extends BaseAdapter {

    ArrayList<Product2> list;
    Context context;
    DatabaseHandler databaseHandler;
    private RequestQueue requestQueue;
    private ImageLoader mImageLoader;

    private static final String RUPEE = "\u20B9 ";

    public ProductsAdapter(Context context, ArrayList<Product2> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getProductId();
    }

    /**
     * Class to hold the view of a single row view
     */
    class ViewHolder {

        NetworkImageView ivThumb;
        TextView tvTitle;
        TextView tvPrice;
        ImageView ivFav;
        ImageView ivCart;

        ViewHolder(View v) {
            ivThumb = (NetworkImageView) v.findViewById(R.id.thumb);
            tvTitle = (TextView) v.findViewById(R.id.tvTitle);
            tvPrice = (TextView) v.findViewById(R.id.tvPrice);
            ivFav = (ImageView) v.findViewById(R.id.ivFav);
            ivCart = (ImageView) v.findViewById(R.id.ivCart);
        }
    }

    /**
     * This overrided method is used to create view for each row of list dynamically
     * of list dynamically
     *
     * @param position    The position of the item within the adapter's data set of the item whose view we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view is non-null
     *                    and of an appropriate type before using. If it is not possible to convert this view to
     *                    display the correct data, this method can create a new view. Heterogeneous lists can
     *                    specify their number of view types, so that this View is always of the right type.
     * @param parent      The parent that this view will eventually be attached to.
     * @return View This returns view of a single file.
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View row = convertView;
        ViewHolder holder = null;

        requestQueue = VolleySingleton.getInstance().getRequestQueue();
        mImageLoader = VolleySingleton.getInstance().getImageLoader();

        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.products_list_item, parent, false);
            holder = new ViewHolder(row);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        Product2 temp = list.get(position);
        holder.ivThumb.setImageUrl(temp.getImageUrl(), mImageLoader);
        holder.tvTitle.setText(temp.getProductShortName());
        holder.tvPrice.setText(temp.getProductPrice().toString());
        holder.ivFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseHandler = new DatabaseHandler(context);
                Product2 selected = list.get(position);
                if (databaseHandler.addToFavourites(new FavouriteItem(selected.getProductId(), selected.getShopId(), selected.getProductShortName(), selected.getProductPrice().toDouble(), R.drawable.sample_product))) {
                    Toast.makeText(context, "Added to Favourites", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(StoriCart.getAppContext(), "Already in Favourites", Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.ivCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.product_popup);
                dialog.show();
            }
        });
        return row;
    }
}
