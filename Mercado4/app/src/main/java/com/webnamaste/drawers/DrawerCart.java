package com.webnamaste.drawers;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.webnamaste.adapters.DrawerAdapter;
import com.webnamaste.mercado.AccountActivity;
import com.webnamaste.mercado.CartActivity;
import com.webnamaste.mercado.FavouritesActivity;
import com.webnamaste.mercado.HistoryActivity;
import com.webnamaste.mercado.R;
import com.webnamaste.mercado.SelectLocationActivity;
import com.webnamaste.offline.OfflineProductCategoriesActivity;
import com.webnamaste.pojo.DrawerItem;

import java.util.ArrayList;


/**
 * The Navigation Drawer Fragment that displays navigation drawer
 *
 * @author  Sushin PS
 * @version 1.0
 * @since   2015-06-10
 */
public class DrawerCart extends Fragment {

    /**
     * Static Variables.
     */
    public static final String PREF_FILE_NAME="mercadopref";
    public static final String KEY_USER_LEARNED_DRAWER="user_learned_drawer";


    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;

    private boolean mUserLearnedDrawer;
    private boolean mFromSavedInstanceState;

    private View containerView;

    ListView listView;
    ArrayList<DrawerItem> list;

    public DrawerCart() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserLearnedDrawer = Boolean.valueOf(readFromPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, "false"));
        if(savedInstanceState!=null){
            mFromSavedInstanceState = true;
        }

    }

    private void getDrawerItems() {

        list = new ArrayList<DrawerItem>();

//        DrawerItem itemHome = new DrawerItem(1,"Account/Login",R.drawable.menu_icon_home);
//        list.add(itemHome);
        DrawerItem itemCart = new DrawerItem(2,"Cart",R.drawable.menu_icon_cart);
        list.add(itemCart);
        DrawerItem itemChangeLocation = new DrawerItem(3,"Change Location",R.drawable.menu_icon_location);
        list.add(itemChangeLocation);
        DrawerItem itemFavourites = new DrawerItem(4,"Favourites",R.drawable.menu_icon_fav);
        list.add(itemFavourites);
        DrawerItem itemHistory = new DrawerItem(5,"Orders",R.drawable.menu_icon_history);
        list.add(itemHistory);
        DrawerItem itemRate = new DrawerItem(6,"Offline Mode",R.drawable.cloud_offline);
        list.add(itemRate);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        listView = (ListView) rootView.findViewById(R.id.listView);
        getDrawerItems();
        listView.setAdapter(new DrawerAdapter(getActivity().getApplicationContext(), list));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DrawerItem selectedItem = list.get(position);
                switch (selectedItem.getId()){
                    case 1:
                        Intent intentHome = new Intent(rootView.getContext(),AccountActivity.class);
                        startActivity(intentHome);
                        mDrawerLayout.closeDrawers();
                        break;
                    case 2:
                        Intent intentCart = new Intent(rootView.getContext(),CartActivity.class);
                        startActivity(intentCart);
                        mDrawerLayout.closeDrawers();
                        break;
                    case 3:
                        Intent intentChangeLocation = new Intent(rootView.getContext(),SelectLocationActivity.class);
                        startActivity(intentChangeLocation);
                        mDrawerLayout.closeDrawers();
                        break;
                    case 4:
                        Intent intentFavourites = new Intent(rootView.getContext(),FavouritesActivity.class);
                        startActivity(intentFavourites);
                        mDrawerLayout.closeDrawers();
                        break;
                    case 5:
                        Intent intentHistory = new Intent(rootView.getContext(),HistoryActivity.class);
                        startActivity(intentHistory);
                        mDrawerLayout.closeDrawers();
                        break;
                    case 6:
                        Intent intent = new Intent(rootView.getContext(), OfflineProductCategoriesActivity.class);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        break;
                    default:
                        mDrawerLayout.closeDrawers();
                        break;
                }
            }
        });
        return rootView;
    }


    /**
     * Method to setup drawer
     *
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout, Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout=drawerLayout;
        mDrawerToggle=new ActionBarDrawerToggle(getActivity(),drawerLayout,toolbar,R.string.drawer_open,R.string.drawer_close){

            @Override
            public void onDrawerOpened(View drawerView) {
                if(!mUserLearnedDrawer){
                    mUserLearnedDrawer=true;
                    saveToPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, mUserLearnedDrawer+"");
                }
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                getActivity().invalidateOptionsMenu();
            }

        };

        if(!mUserLearnedDrawer && !mFromSavedInstanceState){

        }

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    public static void saveToPreferences(Context context, String preferenceName, String preferenceValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue);
        editor.apply();
    }

    public static String readFromPreferences(Context context, String preferenceName, String defaultValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName, defaultValue);
    }
}
