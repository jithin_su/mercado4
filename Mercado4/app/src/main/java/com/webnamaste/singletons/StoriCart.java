package com.webnamaste.singletons;

import android.app.Application;
import android.content.Context;

/**
 * Created by sushinps on 21/06/15.
 */
public class StoriCart extends Application {

    protected static final String TAG = "storicart";

    private static StoriCart mInstance;
    private static Context mAppContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        this.setAppContext(getApplicationContext());
    }

    public static StoriCart getInstance(){
        return mInstance;
    }

    public static Context getAppContext() {
        return mAppContext;
    }

    public void setAppContext(Context mAppContext) {
        this.mAppContext = mAppContext;
    }

}
