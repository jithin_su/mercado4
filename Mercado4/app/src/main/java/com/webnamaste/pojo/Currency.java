package com.webnamaste.pojo;

/**
 * Created by sushinps on 06/06/15.
 */
public class Currency {

    String type;
    String value;

    public Currency(String type, String value) {
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return value;
    }
}
