package com.webnamaste.pojo;

/**
 * Created by sushinps on 25/06/15.
 */
public class OfflineProduct {

    int productId;
    int categoryId;
    int shopId;
    String productShortName;
    String productLongName;
    String productIconUrl;
    String productImageUrl;
    String productDescription;
    UnitOfMeasurement unitOfMeasurement;
    ProductPrice productPrice;

    public OfflineProduct(int productId, int categoryId,int shopId, String productShortName, String productLongName, String productIconUrl, String productImageUrl, String productDescription, UnitOfMeasurement unitOfMeasurement, ProductPrice productPrice) {
        this.productId = productId;
        this.categoryId = categoryId;
        this.shopId = shopId;
        this.productShortName = productShortName;
        this.productLongName = productLongName;
        this.productIconUrl = productIconUrl;
        this.productImageUrl = productImageUrl;
        this.productDescription = productDescription;
        this.unitOfMeasurement = unitOfMeasurement;
        this.productPrice = productPrice;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductShortName() {
        return productShortName;
    }

    public void setProductShortName(String productShortName) {
        this.productShortName = productShortName;
    }

    public String getProductLongName() {
        return productLongName;
    }

    public void setProductLongName(String productLongName) {
        this.productLongName = productLongName;
    }

    public String getProductIconUrl() {
        return productIconUrl;
    }

    public void setProductIconUrl(String productIconUrl) {
        this.productIconUrl = productIconUrl;
    }

    public String getProductImageUrl() {
        return productImageUrl;
    }

    public void setProductImageUrl(String productImageUrl) {
        this.productImageUrl = productImageUrl;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public UnitOfMeasurement getUnitOfMeasurement() {
        return unitOfMeasurement;
    }

    public void setUnitOfMeasurement(UnitOfMeasurement unitOfMeasurement) {
        this.unitOfMeasurement = unitOfMeasurement;
    }

    public ProductPrice getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(ProductPrice productPrice) {
        this.productPrice = productPrice;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }
}
