package com.webnamaste.pojo;

/**
 * Created by sushinps on 19/06/15.
 */
public class FavouriteItem {

    int productId;
    int shopId;
    String productName;
    double productPrice;
    int imageId;

    public FavouriteItem(int productId, int shopId, String productName, double productPrice, int imageId) {
        this.productId = productId;
        this.shopId = shopId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.imageId = imageId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
