package com.webnamaste.pojo;

import java.util.ArrayList;

/**
 * Created by sushinps on 29/06/15.
 */
public class HistoryOrder {
    int orderId;
    int shopId;
    String orderedDate, deliveryTime;
    String status;
    Address shipToAddress;
    ArrayList<Item> items;

    public HistoryOrder(){

    }
    public HistoryOrder(int orderId, int shopId, String orderedDate, String deliveryTime, String status, Address shipToAddress, ArrayList<Item> items) {
        this.orderId = orderId;
        this.shopId = shopId;
        this.orderedDate = orderedDate;
        this.deliveryTime = deliveryTime;
        this.status = status;
        this.shipToAddress = shipToAddress;
        this.items = items;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getOrderedDate() {
        return orderedDate;
    }

    public void setOrderedDate(String orderedDate) {
        this.orderedDate = orderedDate;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Address getShipToAddress() {
        return shipToAddress;
    }

    public void setShipToAddress(Address shipToAddress) {
        this.shipToAddress = shipToAddress;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }
}
