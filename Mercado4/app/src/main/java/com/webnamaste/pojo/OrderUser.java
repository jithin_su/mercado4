package com.webnamaste.pojo;

/**
 * Created by sushinps on 17/07/15.
 */
public class OrderUser {
    String  userId;
    String password;
    String phone;

    public OrderUser() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public OrderUser(String userId, String password, String phone) {
        this.userId = userId;
        this.password = password;
        this.phone = phone;
    }
}
