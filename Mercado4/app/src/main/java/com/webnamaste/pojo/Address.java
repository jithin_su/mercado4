package com.webnamaste.pojo;

/**
 * Created by sushinps on 21/06/15.
 */
public class Address {

    int addressId;
    String addressLine1;
    String addressLine2;
    String street;
    String city;
    String state;
    String country;
    String zip;
/*
    public Address(String addressLine1, String city,String street, String state, String country, String zip) {
        this.addressLine1 = addressLine1;

        this.street = street;
        this.state = state;
        this.country = country;
        this.zip = zip;
        this.city = city;

    }*/
    public Address(int addressId,String addressLine1, String addressLine2, String street, String city, String state, String country, String zip) {
        this.addressId = addressId;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.street = street;
        this.city = city;
        this.state = state;
        this.country = country;
        this.zip = zip;
    }

    public Address(String addressLine1, String addressLine2, String street, String city, String state, String country, String zip) {
        this.addressId = addressId;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.street = street;
        this.city = city;
        this.state = state;
        this.country = country;
        this.zip = zip;
    }


    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public boolean validateAddress(){
        return true;
    }

    @Override
    public String toString() {
        return addressLine1 + "\n" + addressLine2 + "\n" + street + "\n" + city + "\n" + state + "\n" + country + "\n" + zip;
    }
}
