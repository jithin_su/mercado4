package com.webnamaste.pojo;

import java.util.List;

/**
 * Created by jithinsu on 05/08/15.
 */
public class OrderForm {


    List<OrderToSave> orderToSaveList;

    public List<OrderToSave> getOrderToSaveList() {
        return orderToSaveList;
    }

    public void setOrderToSaveList(List<OrderToSave> orderToSaveList) {
        this.orderToSaveList = orderToSaveList;
    }
}
