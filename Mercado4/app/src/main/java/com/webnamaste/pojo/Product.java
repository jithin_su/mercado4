package com.webnamaste.pojo;

import com.webnamaste.mercado.R;

/**
 * Created by sushinps on 06/06/15.
 */
public class Product {

    int productId;
    int shopId;
    String productShortName;
    String productLongName;
    String icon;
    String image;
    String description;
    UnitOfMeasurement uom;
    ProductPrice productPrice;

    public Product(int productId, int shopId, String productShortName, String productLongName, String icon, String image, String description, UnitOfMeasurement uom, ProductPrice productPrice) {
        this.productId = productId;
        this.shopId = shopId;
        this.productShortName = productShortName;
        this.productLongName = productLongName;
        this.icon = icon;
        this.image = image;
        this.description = description;
        this.uom = uom;
        this.productPrice = productPrice;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getProductShortName() {
        return productShortName;
    }

    public void setProductShortName(String productShortName) {
        this.productShortName = productShortName;
    }

    public String getProductLongName() {
        return productLongName;
    }

    public void setProductLongName(String productLongName) {
        this.productLongName = productLongName;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UnitOfMeasurement getUom() {
        return uom;
    }

    public void setUom(UnitOfMeasurement uom) {
        this.uom = uom;
    }

    public ProductPrice getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(ProductPrice productPrice) {
        this.productPrice = productPrice;
    }
}