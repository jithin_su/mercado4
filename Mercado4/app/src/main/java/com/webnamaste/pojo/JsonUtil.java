package com.webnamaste.pojo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by jithinsu on 04/08/15.
 */
public class JsonUtil {


    public static String orderToJSon(OrderForm orderForm) {

        try {

            JSONObject jsonObject = new JSONObject();
            // Here we convert Java Object to JSON
            JSONArray jsonOderArray = new JSONArray();

            for ( OrderToSave orderToSave : orderForm.getOrderToSaveList()) {

                JSONObject orderJsonObj = new JSONObject();

               //////Header ////
                JSONObject headerJsonObj = new JSONObject();
                headerJsonObj.put("action",orderToSave.getHeader().getAction());
                headerJsonObj.put("orderId",orderToSave.getHeader().getOrderId());

                ///USER inside header
                JSONObject userJson = new JSONObject();
                userJson.put("userId",orderToSave.getHeader().getUser().getUserId());
                userJson.put("password",orderToSave.getHeader().getUser().getPassword());
                userJson.put("phone",orderToSave.getHeader().getUser().getPhone());

                ///SHOP inside

                JSONObject shopJson = new JSONObject();
                shopJson.put("shopId",orderToSave.getHeader().getShop().getShopId());
                shopJson.put("phone",orderToSave.getHeader().getShop().getPhone());

                ///STATUS inside
                JSONObject statusJson = new JSONObject();
                statusJson.put("message",orderToSave.getHeader().getStatus().getMessage());
                statusJson.put("value", orderToSave.getHeader().getStatus().getValue());

                ////SHIPPING ADDRESSS
                JSONObject shippingAdressJson = new JSONObject();
                shippingAdressJson.put("addressId", orderToSave.getHeader().getShipToAddress().getAddressId());
                shippingAdressJson.put("adLine1", orderToSave.getHeader().getShipToAddress().getAddressLine1());

                shippingAdressJson.put("city", orderToSave.getHeader().getShipToAddress().getCity());
                shippingAdressJson.put("street",orderToSave.getHeader().getShipToAddress().getStreet());
                shippingAdressJson.put("state",orderToSave.getHeader().getShipToAddress().getState());
                shippingAdressJson.put("country", orderToSave.getHeader().getShipToAddress().getCountry());
                shippingAdressJson.put("zip",orderToSave.getHeader().getShipToAddress().getZip());

                ///Adding inner class to header
                headerJsonObj.put("user",userJson);
                headerJsonObj.put("shop",shopJson);
                headerJsonObj.put("status",statusJson);
                headerJsonObj.put("shipToAddress",shippingAdressJson);


                orderJsonObj.put("header",headerJsonObj);


                ////Here managing Lists of items
                JSONArray jsonItemArray = new JSONArray();
                for ( OrderItem item : orderToSave.getOrderItemList()) {
                    JSONObject itemJsonObject = new JSONObject();
                    itemJsonObject.put("orderItemId",item.getOrderItemId());
                    itemJsonObject.put("skuId", item.getSkuId());
                    itemJsonObject.put("productId", item.getProductId());
                    itemJsonObject.put("productShortName",item.getProductShortName());
                    itemJsonObject.put("productLongName",item.getProductLongName());
                    itemJsonObject.put("quantity",item.getQuantity());
                    itemJsonObject.put("uom","item");
                    ///STATUS inside
                    JSONObject statusJsonitem = new JSONObject();
                    statusJsonitem.put("message",orderToSave.getHeader().getStatus().getMessage());
                    statusJsonitem.put("value", orderToSave.getHeader().getStatus().getValue());

                    /// price inside

                    JSONObject pricejson = new JSONObject();
                    pricejson.put("actual",item.getPrice().getActual());
                    pricejson.put("currency",item.getPrice().getCurrency());


                    itemJsonObject.put("status",statusJsonitem);
                    itemJsonObject.put("price",pricejson);
                    jsonItemArray.put(itemJsonObject);

                }
                JSONObject item = new JSONObject();
                item.put("item",jsonItemArray);

                orderJsonObj.put("items",item);

                /////////////////////////////////

                jsonOderArray.put(orderJsonObj);
              //  jsonOderArray.

                //orderToSave.getHeader().

               // jsonOderArray.put(orderToSave);
            }

            jsonObject.put("order",jsonOderArray);

            return jsonObject.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String toJSon(Registration registration) {

        try {

            JSONObject jsonObject = new JSONObject();
            // Here we convert Java Object to JSON
            JSONArray jsonArr = new JSONArray();

            for (Address pn : registration.getAddressLists()) {
                JSONObject pnObj = new JSONObject();
                pnObj.put("adLine1", pn.getAddressLine1());
                pnObj.put("city", pn.getCity());
                pnObj.put("street",pn.getStreet());
                pnObj.put("state",pn.getState());
                pnObj.put("country", pn.getCountry());
                pnObj.put("zip",pn.getZip());
                jsonArr.put(pnObj);
            }

            jsonObject.put("address", jsonArr);

            JSONObject jsondetail = new JSONObject(); // we need another object to store the address
            jsondetail.put("firstName", registration.getDetails().getFirstName());
            jsondetail.put("lastName", registration.getDetails().getLastName());
            jsondetail.put("email", registration.getDetails().getEmail());
            jsondetail.put("dateOfBirth",registration.getDetails().getDateOfBirth());
            jsondetail.put("deviceId",registration.getDetails().getDeviceId());
            jsondetail.put("langId",registration.getDetails().getLangId());
            jsondetail.put("userType",registration.getDetails().getUserType());

            jsonObject.put("details", jsondetail);

            JSONObject jsonheader = new JSONObject();
            jsonheader.put("action", registration.getHeader().getAction()); // Set the first name/pair
            jsonheader.put("userId", registration.getHeader().getUserid());

            jsonheader.put("password", registration.getHeader().getPassword());
            jsonheader.put("phone",registration.getHeader().getPhnumber());
            jsonObject.put("header", jsonheader);

            return jsonObject.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}