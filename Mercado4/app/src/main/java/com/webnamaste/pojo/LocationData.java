package com.webnamaste.pojo;

/**
 * Created by sushinps on 21/06/15.
 */
public class LocationData {

    boolean isGPS;
    double latitude;
    double longitude;
    String name;

    public LocationData(boolean isGPS, double latitude, double longitude, String name) {
        this.isGPS = isGPS;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
    }

    public boolean isGPS() {
        return isGPS;
    }

    public void setIsGPS(boolean isGPS) {
        this.isGPS = isGPS;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
