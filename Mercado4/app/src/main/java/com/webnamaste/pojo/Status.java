package com.webnamaste.pojo;

/**
 * Created by sushinps on 17/07/15.
 */
public class Status {
    String message;
    String value;

    public Status(String message, String value) {
        this.message = message;
        this.value = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
