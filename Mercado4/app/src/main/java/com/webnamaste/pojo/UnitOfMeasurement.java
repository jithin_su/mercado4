package com.webnamaste.pojo;

/**
 * Created by sushinps on 06/06/15.
 */
public class UnitOfMeasurement {

    String description;
    Boolean isFloat;
    String value;

    public UnitOfMeasurement(String description, Boolean isFloat, String value) {
        this.description = description;
        this.isFloat = isFloat;
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsFloat() {
        return isFloat;
    }

    public void setIsFloat(Boolean isFloat) {
        this.isFloat = isFloat;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
