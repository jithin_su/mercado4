package com.webnamaste.pojo;

/**
 * Created by sushinps on 20/06/15.
 */
public class Shop {

    int shopId;
    String shopName;
    String orderSendToPhone;
    double latitude, longitude;
    double minOrderAmount;
    int minTimeToDeliver;
    String imageUrlSmall;
    String getImageUrlLarge;

    public Shop(int shopId, String shopName, String orderSendToPhone, double latitude, double longitude, double minOrderAmount, int minTimeToDeliver, String imageUrlSmall, String getImageUrlLarge) {
        this.shopId = shopId;
        this.shopName = shopName;
        this.orderSendToPhone = orderSendToPhone;
        this.latitude = latitude;
        this.longitude = longitude;
        this.minOrderAmount = minOrderAmount;
        this.minTimeToDeliver = minTimeToDeliver;
        this.imageUrlSmall = imageUrlSmall;
        this.getImageUrlLarge = getImageUrlLarge;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getOrderSendToPhone() {
        return orderSendToPhone;
    }

    public void setOrderSendToPhone(String orderSendToPhone) {
        this.orderSendToPhone = orderSendToPhone;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getMinOrderAmount() {
        return minOrderAmount;
    }

    public void setMinOrderAmount(double minOrderAmount) {
        this.minOrderAmount = minOrderAmount;
    }

    public int getMinTimeToDeliver() {
        return minTimeToDeliver;
    }

    public void setMinTimeToDeliver(int minTimeToDeliver) {
        this.minTimeToDeliver = minTimeToDeliver;
    }

    public String getImageUrlSmall() {
        return imageUrlSmall;
    }

    public void setImageUrlSmall(String imageUrlSmall) {
        this.imageUrlSmall = imageUrlSmall;
    }

    public String getGetImageUrlLarge() {
        return getImageUrlLarge;
    }

    public void setGetImageUrlLarge(String getImageUrlLarge) {
        this.getImageUrlLarge = getImageUrlLarge;
    }
}
