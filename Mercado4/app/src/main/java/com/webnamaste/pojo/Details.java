package com.webnamaste.pojo;

/**
 * Created by jithinsu on 04/08/15.
 */
public class Details
{
    private String  firstName ;// : "mmmm first name"
    private String  lastName ;// : "lllll lasst naame"
    private String  dateOfBirth; // : "2015/12/15"
    private String  email ; //= ";laksjdf;laksjf"
    private String  deviceId; //=] "asdlfakjsldfjlasdjflk"
    private String  langId ;//: "1"
    private String  userType ; // : "EU"

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getLangId() {
        return langId;
    }

    public void setLangId(String langId) {
        this.langId = langId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}