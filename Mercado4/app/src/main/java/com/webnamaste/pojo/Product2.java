package com.webnamaste.pojo;

/**
 * Created by sushinps on 21/06/15.
 */
public class Product2 {

    int productId;
    int shopId;
    String productShortName;
    String productLongName;
    UnitOfMeasurement uom;
    ProductPrice productPrice;

    public String imageUrl;

    public Product2(int productId, int shopId, String productShortName, String productLongName, UnitOfMeasurement uom, ProductPrice productPrice, String imageUrl) {
        this.productId = productId;
        this.shopId = shopId;
        this.productShortName = productShortName;
        this.productLongName = productLongName;
        this.uom = uom;
        this.productPrice = productPrice;
        this.imageUrl = imageUrl;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getProductShortName() {
        return productShortName;
    }

    public void setProductShortName(String productShortName) {
        this.productShortName = productShortName;
    }

    public String getProductLongName() {
        return productLongName;
    }

    public void setProductLongName(String productLongName) {
        this.productLongName = productLongName;
    }

    public UnitOfMeasurement getUom() {
        return uom;
    }

    public void setUom(UnitOfMeasurement uom) {
        this.uom = uom;
    }

    public ProductPrice getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(ProductPrice productPrice) {
        this.productPrice = productPrice;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
