package com.webnamaste.pojo;

/**
 * Created by sushinps on 14/06/15.
 */
public class DrawerItem {

    int id;
    String title;
    int imageId;

    public DrawerItem(int id, String title, int imageId) {
        this.id = id;
        this.title = title;
        this.imageId = imageId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
