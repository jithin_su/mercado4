package com.webnamaste.pojo;

/**
 * Created by sushinps on 06/06/15.
 */
public class ProductPrice {

    double actual;
    double offer;
    String currency;

    private static final String RUPEE = "\u20B9 ";

    public ProductPrice(double actual, String currency) {
        this.actual = actual;
        this.currency = currency;
        this.offer = 0;
    }

    public ProductPrice(double actual) {
        this.actual = actual;
        this.currency = "INR";
        this.offer = 0;
    }

    public ProductPrice(double actual, double offer, String currency){
        this.actual = actual;
        this.offer = offer;
        this.currency = currency;
    }

    public boolean hasOffer(){
        if(offer == 0){
            return false;
        } else {
            return true;
        }
    }

    public void setActual(double actual) {
        this.actual = actual;
    }

    public double getOffer() {
        return offer;
    }

    public void setOffer(double offer) {
        this.offer = offer;
    }

    public double getActual() {
        return actual;
    }

    public void setActual(float actual) {
        this.actual = actual;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return RUPEE+" "+actual;
    }

    public double toDouble() {
        if(hasOffer()){
            return offer;
        } else {
            return actual;
        }
    }
}
