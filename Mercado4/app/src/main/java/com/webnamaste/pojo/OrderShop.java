package com.webnamaste.pojo;

/**
 * Created by sushinps on 17/07/15.
 */
public class OrderShop {
    String shopId;
    String phone;

    public OrderShop(String shopId, String phone) {
        this.shopId = shopId;
        this.phone = phone;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
