package com.webnamaste.pojo;

/**
 * Created by sushinps on 15/05/15.
 */
public class Category {

    public int categoryId;
    public int imageId;
    public String title;

    public Category(int imageId, String title){
        this.imageId = imageId;
        this.title = title;
    }

    public Category(int categoryId, int imageId, String title) {
        this.categoryId = categoryId;
        this.imageId = imageId;
        this.title = title;
    }
}
