package com.webnamaste.pojo;

/**
 * Created by sushinps on 19/06/15.
 */
public class CartItem {

    int productId;
    int shopId;
    String productName;
    double productPrice;
    int quantity;
    int imageId;

    public CartItem(int productId, int shopId, String productName, double productPrice, int quantity, int imageId) {
        this.productId = productId;
        this.shopId = shopId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.quantity = quantity;
        this.imageId = imageId;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void incrementQuantity() { this.quantity++; }

    public void decrementQuantity() { this.quantity--; }
}
