package com.webnamaste.pojo;

/**
 * Created by sushinps on 08/07/15.
 */
public class Item {

    private int productId;
    int shopId;
    private String productShortName;
    private String productLongName;
    private ProductPrice price;
    private String status;
    private String uom;
    private int orderItemId;
    private int quantity;

    public Item(int productId, int shopId, String productShortName, String productLongName, ProductPrice price, String status, String uom, int orderItemId, int quantity) {
        this.productId = productId;
        this.shopId = shopId;
        this.productShortName = productShortName;
        this.productLongName = productLongName;
        this.price = price;
        this.status = status;
        this.uom = uom;
        this.orderItemId = orderItemId;
        this.quantity = quantity;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getProductShortName() {
        return productShortName;
    }

    public void setProductShortName(String productShortName) {
        this.productShortName = productShortName;
    }

    public String getProductLongName() {
        return productLongName;
    }

    public void setProductLongName(String productLongName) {
        this.productLongName = productLongName;
    }

    public ProductPrice getPrice() {
        return price;
    }

    public void setPrice(ProductPrice price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public int getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(int orderItemId) {
        this.orderItemId = orderItemId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
