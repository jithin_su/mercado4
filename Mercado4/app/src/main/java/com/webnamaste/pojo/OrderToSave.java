package com.webnamaste.pojo;

import java.util.List;

/**
 * Created by sushinps on 17/07/15.
 */
public class OrderToSave {

    OrderHeader header;
    List<OrderItem> orderItemList;

    public OrderHeader getHeader() {
        return header;
    }

    public void setHeader(OrderHeader header) {
        this.header = header;
    }

    public List<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(List<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }
}
