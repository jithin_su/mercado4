package com.webnamaste.pojo;

/**
 * Created by Sushin PS on 8/5/2015.
 */
public class CartItem3 {
    int id;
    int productId;
    int shopId;
    String productShortName;
    String productLongName;
    int quantity;
    String uom;
    double actualPrice;
    double offerPrice;

    public CartItem3(int id, int productId, int shopId, String productShortName, String productLongName, int quantity, String uom, double actualPrice, double offerPrice) {
        this.id = id;
        this.productId = productId;
        this.shopId = shopId;
        this.productShortName = productShortName;
        this.productLongName = productLongName;
        this.quantity = quantity;
        this.uom = uom;
        this.actualPrice = actualPrice;
        this.offerPrice = offerPrice;
    }
}
