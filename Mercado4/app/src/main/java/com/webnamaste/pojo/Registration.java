package com.webnamaste.pojo;

import java.util.List;

/**
 * Created by jithinsu on 04/08/15.
 */
public class Registration {

    private Header  header;

    private Details details ;
    private List<Address> addressLists;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public List<Address> getAddressLists() {
        return addressLists;
    }

    public void setAddressLists(List<Address> addressLists) {
        this.addressLists = addressLists;
    }

// get and set







    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }
}
