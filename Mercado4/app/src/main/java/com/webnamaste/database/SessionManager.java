package com.webnamaste.database;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.webnamaste.mercado.LoginActivity;
import com.webnamaste.pojo.Address;
import com.webnamaste.pojo.LocationData;
import com.webnamaste.pojo.Shop;
import com.webnamaste.pojo.User;

/**
 * Created by sushinps on 21/06/15.
 */
public class SessionManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "StoriLabsPref";
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String IS_FIRST_USE = "IsFirstUse";
    public static final String IS_LOCATION_SET = "IsLocationSet";
    public static final String KEY_IS_GPS = "IsGPS";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_LOCATION_NAME = "locationName";

    public static final String KEY_USER_ID = "userId";
    public static final String KEY_FIRST_NAME = "firstName";
    public static final String KEY_LAST_NAME = "lastName";
    public static final String KEY_DATE_OF_BIRTH = "dateOfBirth";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_EMAIL_ID = "emailId";
    public static final String KEY_LOGON_ID = "logonId";
    public static final String KEY_PASSWORD = "password";

    public static final String KEY_ADDRESS_1 = "addressLine1";
    public static final String KEY_ADDRESS_2 = "addressLine2";
    public static final String KEY_STREET = "street";
    public static final String KEY_CITY = "city";
    public static final String KEY_STATE = "state";
    public static final String KEY_COUNTRY = "country";
    public static final String KEY_ZIP = "zip";

    private static final String IS_OFFLINE_SET = "IsOfflineSet";
    private static final String KEY_SHOP_ID = "shopId";
    private static final String KEY_SHOP_NAME = "shopName";
    private static final String KEY_SHOP_PHONE = "shopPhone";

    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createMobileAndPassword(String mobileNumber, String password) {
        editor.putString(KEY_PHONE, mobileNumber);
        editor.putString(KEY_PASSWORD, password);
        editor.commit();
    }

    public String getMobileNumber() {
        return pref.getString(KEY_PHONE, "");
    }

    public String getPassword() {
        return pref.getString(KEY_PASSWORD, "");
    }

    public void setLoginIn(int userId) {
        editor.putBoolean(IS_LOGIN, true);

        editor.putInt(KEY_USER_ID, userId);
        editor.commit();
    }

    public int getUserId() {
        if(isLoggedIn()) {
            return pref.getInt(KEY_USER_ID, 0);
        } else {
            return 0;
        }
    }

    public void setLoginOut() {
        editor.putBoolean(IS_LOGIN, false);
        editor.commit();
    }

    public void createLoginSession(User user) {

        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        editor.putInt(KEY_USER_ID, user.getUserId());
        editor.putString(KEY_FIRST_NAME, user.getFirstName());
        editor.putString(KEY_LAST_NAME, user.getLastName());
        editor.putString(KEY_DATE_OF_BIRTH, user.getDateOfBirth());
        editor.putString(KEY_PHONE, user.getPhone());
        editor.putString(KEY_EMAIL_ID, user.getEmailId());
        editor.putString(KEY_LOGON_ID, user.getLogonId());
        editor.putString(KEY_PASSWORD, user.getPassword());

        Address address = user.getAddress();
        editor.putString(KEY_ADDRESS_1, address.getAddressLine1());
        editor.putString(KEY_ADDRESS_2, address.getAddressLine2());
        editor.putString(KEY_STREET, address.getStreet());
        editor.putString(KEY_CITY, address.getCity());
        editor.putString(KEY_STATE, address.getState());
        editor.putString(KEY_COUNTRY, address.getCountry());
        editor.putString(KEY_ZIP, address.getZip());

        // commit changes
        editor.commit();
    }

    public User getUserDetails() {
        Address address = new Address(pref.getString(KEY_ADDRESS_1, null), pref.getString(KEY_ADDRESS_2, null), pref.getString(KEY_STREET, null), pref.getString(KEY_CITY, null), pref.getString(KEY_STATE, null), pref.getString(KEY_COUNTRY, null), pref.getString(KEY_ZIP, null));
        User user = new User(pref.getInt(KEY_USER_ID, 0), pref.getString(KEY_FIRST_NAME, null), pref.getString(KEY_LAST_NAME, null), pref.getString(KEY_DATE_OF_BIRTH, null), pref.getString(KEY_PHONE, null), pref.getString(KEY_EMAIL_ID, null), pref.getString(KEY_LOGON_ID, null), pref.getString(KEY_PASSWORD, null), address);

        // return user
        return user;
    }

    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }

    }

    public void markFirstUse() {

        // Storing login value as TRUE
        editor.putBoolean(IS_FIRST_USE, false);

        // commit changes
        editor.commit();
    }

    public void setLocation(LocationData locationData) {

        // Storing login value as TRUE
        editor.putBoolean(IS_LOCATION_SET, true);
        editor.putBoolean(KEY_IS_GPS, locationData.isGPS());
        editor.putLong(KEY_LATITUDE, Double.doubleToLongBits(locationData.getLatitude()));
        editor.putLong(KEY_LONGITUDE, Double.doubleToLongBits(locationData.getLatitude()));
        editor.putString(KEY_LOCATION_NAME, locationData.getName());

        // commit changes
        editor.commit();
    }

    public LocationData getLocationDetails() {
        LocationData locationData;
        locationData = new LocationData(pref.getBoolean(KEY_IS_GPS, false), Double.longBitsToDouble(pref.getLong(KEY_LATITUDE, 0)), Double.longBitsToDouble(pref.getLong(KEY_LONGITUDE, 0)), pref.getString(KEY_LOCATION_NAME, ""));
        return locationData;
    }

    public boolean isLoggedIn() {
//        return true;
        return pref.getBoolean(IS_LOGIN, false);
    }

    public boolean isFirstUse() {
        return pref.getBoolean(IS_FIRST_USE, true);
    }

    public boolean isLocationSet() {
        return pref.getBoolean(IS_LOCATION_SET, false);
    }

    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
    }

    /**************************************OFFLINE DATAS***************************************************/

    public boolean isOfflineSet() {
        return pref.getBoolean(IS_OFFLINE_SET, false);
    }

    public void setOfflineData(Shop shop) {

        // Storing login value as TRUE
        editor.putBoolean(IS_OFFLINE_SET, true);
        editor.putInt(KEY_SHOP_ID, shop.getShopId());
        editor.putString(KEY_SHOP_NAME, shop.getShopName());
        editor.putString(KEY_SHOP_PHONE, shop.getOrderSendToPhone());

        // commit changes
        editor.commit();
    }


}
