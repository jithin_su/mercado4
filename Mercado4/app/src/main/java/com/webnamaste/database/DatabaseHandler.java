package com.webnamaste.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.webnamaste.mercado.R;
import com.webnamaste.pojo.Address;
import com.webnamaste.pojo.CartItem;
import com.webnamaste.pojo.CartItem2;
import com.webnamaste.pojo.CartItem3;
import com.webnamaste.pojo.Category;
import com.webnamaste.pojo.FavouriteItem;
import com.webnamaste.pojo.Item;
import com.webnamaste.pojo.OfflineProduct;
import com.webnamaste.pojo.HistoryOrder;
import com.webnamaste.pojo.Product;
import com.webnamaste.pojo.ProductPrice;
import com.webnamaste.pojo.Shop;
import com.webnamaste.pojo.UnitOfMeasurement;

import java.util.ArrayList;

/**
 * Created by sushinps on 19/06/15.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "storilabs";
    private static final int DATABASE_VERSION = 1;


    private static final String KEY_PRODUCT_ID = "product_id";
    private static final String KEY_SHOP_ID = "shop_id";
    private static final String KEY_PRODUCT_NAME = "product_name";
    private static final String KEY_PRODUCT_PRICE = "product_price";
    private static final String KEY_QUANTITY = "quantity";

    private static final String KEY_PRODUCT_SHORT_NAME = "product_short_name";
    private static final String KEY_PRODUCT_LONG_NAME = "product_long_name";
    private static final String KEY_PRODUCT_IMAGE_URL_SMALL = "product_image_url_small";
    private static final String KEY_PRODUCT_IMAGE_URL_LARGE = "product_image_url_large";
    private static final String KEY_PRODUCT_DESCRIPTION = "product_image_url";

    private static final String KEY_CATEGORY_ID = "category_id";
    private static final String KEY_CATEGORY_NAME = "category_name";
    /*
    int productId;
    int shopId;
    String productShortName;
    String productLongName;
    UnitOfMeasurement uom;
    ProductPrice productPrice;
    public String imageUrl;
     */

    private static final String KEY_SHOP_NAME = "shop_name";
    private static final String KEY_SHOP_PHONE = "shop_phone";
    private static final String KEY_SHOP_LATITUDE = "latitude";
    private static final String KEY_SHOP_LONGITUDE = "longitude";
    private static final String KEY_SHOP_MIN_ORDER = "min_order";
    private static final String KEY_SHOP_MIN_DELIVERY_TIME = "min_delivery_time";
    private static final String KEY_SHOP_IMAGE_URL_SMALL = "image_url_small";
    private static final String KEY_SHOP_IMAGE_URL_LARGE = "image_url_large";

    private static final String KEY_ORDER_ID = "order_id";
    private static final String KEY_ORDER_DATE = "order_date";
    private static final String KEY_DELIVERY_TIME = "delivery_time";
    private static final String KEY_STATUS = "status";
    private static final String KEY_HISTORY_ID = "history_id";

    private static final String KEY_ORDER_ITEM_ID = "order_item_id";
    private static final String KEY_ITEM_STATUS = "order_status";

    private static final String KEY_ADDRESS_ID = "address_id";
    private static final String KEY_ADDRESS_LINE_1 = "address_line_1";
    private static final String KEY_ADDRESS_LINE_2 = "address_line_2";
    private static final String KEY_STREET = "street";
    private static final String KEY_CITY = "city";
    private static final String KEY_STATE = "state";
    private static final String KEY_COUNTRY = "country";
    private static final String KEY_ZIP = "zip";

    private static final String KEY_CART_ID = "cart_id";
    private static final String KEY_UOM = "uom";
    private static final String KEY_ACTUAL_PRICE = "actual_price";
    private static final String KEY_OFFER_PRICE = "offer_price";
    private static final String KEY_CURRENCY = "currency";

    // CART TABLE
    private static final String TABLE_CART = "cart";
    private static final String SQL_TABLE_CART = "CREATE TABLE " + TABLE_CART + "(" +
            KEY_CART_ID + " INTEGER PRIMARY KEY," +
            KEY_PRODUCT_ID + " INTEGER," +
            KEY_SHOP_ID + " INTEGER," +
            KEY_PRODUCT_SHORT_NAME + " TEXT," +
            KEY_PRODUCT_LONG_NAME + " TEXT," +
            KEY_QUANTITY + " INTEGER" +
            KEY_UOM + " TEXT," +
            KEY_ACTUAL_PRICE + " DOUBLE," +
            KEY_OFFER_PRICE + " DOUBLE" + ")";

    private static final String TABLE_SHOPS = "shops";
    private static final String SQL_TABLE_SHOPS = "CREATE TABLE " + TABLE_SHOPS + "(" +
            KEY_SHOP_ID + " INTEGER PRIMARY KEY," +
            KEY_SHOP_NAME + " TEXT," +
            KEY_SHOP_PHONE + " TEXT," +
            KEY_SHOP_LATITUDE + " DOUBLE," +
            KEY_SHOP_LONGITUDE + " DOUBLE," +
            KEY_SHOP_MIN_ORDER + " DOUBLE," +
            KEY_SHOP_MIN_DELIVERY_TIME + " INTEGER," +
            KEY_SHOP_IMAGE_URL_SMALL + " TEXT," +
            KEY_SHOP_IMAGE_URL_LARGE + " TEXT" + ")";

    // FAVOURITES TABLE
    private static final String TABLE_FAVOURITES = "favourites";
    private static final String SQL_TABLE_FAVOURITES = "CREATE TABLE " + TABLE_FAVOURITES + "(" +
            KEY_PRODUCT_ID + " INTEGER PRIMARY KEY," +
            KEY_SHOP_ID + " INTEGER," +
            KEY_PRODUCT_NAME + " TEXT," +
            KEY_PRODUCT_PRICE + " DOUBLE" + ")";

    // HISTORY TABLE
    private static final String TABLE_ADDRESS = "address";
    private static final String SQL_TABLE_ADDRESS = "CREATE TABLE " + TABLE_ADDRESS + "(" +
            KEY_ADDRESS_ID + " INTEGER PRIMARY KEY," +
            KEY_ADDRESS_LINE_1 + " TEXT," +
            KEY_ADDRESS_LINE_2 + " TEXT," +
            KEY_STREET + " TEXT," +
            KEY_CITY + " TEXT," +
            KEY_STATE + " TEXT," +
            KEY_COUNTRY + " TEXT," +
            KEY_ZIP + " TEXT" + ")";

    private static final String TABLE_HISTORY = "history";
    private static final String SQL_TABLE_HISTORY = "CREATE TABLE " + TABLE_HISTORY + "(" +
            KEY_HISTORY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            KEY_ORDER_ID + " INTEGER NOT NULL," +
            KEY_SHOP_ID + " INTEGER NOT NULL," +
            KEY_ORDER_DATE + " TEXT NOT NULL," +
            KEY_DELIVERY_TIME + " TEXT NOT NULL," +
            KEY_PRODUCT_ID + " INTEGER NOT NULL," +
            KEY_STATUS + " TEXT NOT NULL," +
            KEY_ADDRESS_ID + " INTEGER NOT NULL," +
            KEY_PRODUCT_SHORT_NAME + " TEXT NOT NULL," +
            KEY_PRODUCT_LONG_NAME + " TEXT NOT NULL," +
            KEY_PRODUCT_PRICE + " DOUBLE NOT NULL," +
            KEY_ITEM_STATUS + " TEXT NOT NULL," +
            KEY_ORDER_ITEM_ID + " INTEGER NOT NULL," +
            KEY_QUANTITY + " INTEGER NOT NULL" + ")";

    private static final String TABLE_OFFLINE_CATEGORIES = "offline_product_categories";
    private static final String SQL_TABLE_OFFLINE_CATEGORIES = "CREATE TABLE " + TABLE_OFFLINE_CATEGORIES + "(" +
            KEY_CATEGORY_ID + " INTEGER PRIMARY KEY," +
            KEY_CATEGORY_NAME + " TEXT" + ")";

    private static final String TABLE_OFFLINE_PRODUCTS = "offline_products";
    private static final String SQL_TABLE_OFFLINE_PRODUCTS = "CREATE TABLE " + TABLE_OFFLINE_PRODUCTS + "(" +
            KEY_PRODUCT_ID + " INTEGER PRIMARY KEY," +
            KEY_CATEGORY_ID + " INTEGER," +
            KEY_SHOP_ID + " INTEGER," +
            KEY_PRODUCT_SHORT_NAME + " TEXT," +
            KEY_PRODUCT_LONG_NAME + " TEXT," +
            KEY_PRODUCT_IMAGE_URL_SMALL + " TEXT," +
            KEY_PRODUCT_IMAGE_URL_LARGE + " TEXT," +
            KEY_PRODUCT_DESCRIPTION + " TEXT," +
            KEY_PRODUCT_PRICE + " DOUBLE" + ")";


    private Context context;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_TABLE_CART);
        db.execSQL(SQL_TABLE_SHOPS);
        db.execSQL(SQL_TABLE_FAVOURITES);
        db.execSQL(SQL_TABLE_ADDRESS);
        db.execSQL(SQL_TABLE_HISTORY);
        db.execSQL(SQL_TABLE_OFFLINE_CATEGORIES);
        db.execSQL(SQL_TABLE_OFFLINE_PRODUCTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CART);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHOPS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FAVOURITES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HISTORY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OFFLINE_CATEGORIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OFFLINE_PRODUCTS);

        onCreate(db);
    }


    /* ----------------------------- CART METHODS ------------------------------------------ */


    public ArrayList<CartItem3> getCartItemsSample(){
        ArrayList<CartItem3> result = new ArrayList<CartItem3>();
        for(int i=1;i<=10;i++){
            result.add(new CartItem3(i, i, i, "productShortName"+i, "productLongName"+i, i+2, "item", 45.0, 43.0));
        }
        return result;
    }

    public boolean addToCart(CartItem cartItem) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PRODUCT_ID, cartItem.getProductId());
        values.put(KEY_SHOP_ID, cartItem.getShopId());
        values.put(KEY_PRODUCT_NAME, cartItem.getProductName());
        values.put(KEY_PRODUCT_PRICE, cartItem.getProductPrice());
        values.put(KEY_QUANTITY, cartItem.getQuantity());

        // Inserting Row
        long result = db.insert(TABLE_CART, null, values);
        db.close();
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean addToCart2(CartItem2 cartItem) {

        SQLiteDatabase db = this.getWritableDatabase();

        Product product = cartItem.getProduct();

        ContentValues values = new ContentValues();
        values.put(KEY_PRODUCT_ID, product.getProductId());
        values.put(KEY_SHOP_ID, product.getShopId());
        values.put(KEY_PRODUCT_NAME, product.getProductShortName());
        values.put(KEY_PRODUCT_PRICE, product.getProductPrice().toDouble());
        values.put(KEY_QUANTITY, cartItem.getQuantity());

        // Inserting Row
        long result = db.insert(TABLE_CART, null, values);
        db.close();
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public void clearCart(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_CART);
        db.close();
    }


    public void deleteFromCart(CartItem cartItem) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CART, KEY_PRODUCT_ID + " = ?",
                new String[]{String.valueOf(cartItem.getProductId())});
        db.close();
    }

    public int updateCart(CartItem cartItem) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PRODUCT_ID, cartItem.getProductId());
        values.put(KEY_SHOP_ID, cartItem.getShopId());
        values.put(KEY_PRODUCT_NAME, cartItem.getProductName());
        values.put(KEY_PRODUCT_PRICE, cartItem.getProductPrice());
        values.put(KEY_QUANTITY, cartItem.getQuantity());

        // updating row
        return db.update(TABLE_CART, values, KEY_PRODUCT_ID + " = ?",
                new String[]{String.valueOf(cartItem.getProductId())});
    }

    public ArrayList<Integer> getCartShopIds(){
        ArrayList<Integer> shopIds = new ArrayList<Integer>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor1 = db.query(true, TABLE_CART, new String[] { KEY_SHOP_ID }, null, null, KEY_SHOP_ID, null, null, null);
        for (cursor1.moveToFirst(); !cursor1.isAfterLast(); cursor1.moveToNext()) {
            shopIds.add(cursor1.getInt(0));
        }
        return shopIds;
    }

    public ArrayList<CartItem> getCartItemsFromShop(int shopId) {

        ArrayList<CartItem> results;
        SQLiteDatabase db = this.getReadableDatabase();

        results = new ArrayList<CartItem>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_CART + "WHERE "+ KEY_SHOP_ID + "=" + shopId, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            CartItem temp = new CartItem(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getDouble(3), cursor.getInt(4), R.drawable.sample_product);
            results.add(temp);
            cursor.moveToNext();
        }
        return results;
    }

    public ArrayList<CartItem> getCartItems() {

        ArrayList<CartItem> results;
        SQLiteDatabase db = this.getReadableDatabase();

        results = new ArrayList<CartItem>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_CART, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            CartItem temp = new CartItem(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getDouble(3), cursor.getInt(4), R.drawable.sample_product);
            results.add(temp);
            cursor.moveToNext();
        }
        return results;
    }

    public double getCartTotal() {
        double result = 0;
        ArrayList<CartItem> list;
        for (CartItem cartItem : list = getCartItems()) {
            int quantity = cartItem.getQuantity();
            double price = cartItem.getProductPrice();
            double total = quantity * price;
            result += total;
        }
        return result;
    }

    /* ----------------------------- SHOP METHODS ------------------------------------------ */

    public boolean addToShops(Shop shop) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_SHOP_ID, shop.getShopId());
        values.put(KEY_SHOP_NAME, shop.getShopName());
        values.put(KEY_SHOP_PHONE, shop.getShopName());
        values.put(KEY_SHOP_LATITUDE, shop.getLatitude());
        values.put(KEY_SHOP_LONGITUDE, shop.getLongitude());
        values.put(KEY_SHOP_MIN_ORDER, shop.getMinOrderAmount());
        values.put(KEY_SHOP_MIN_DELIVERY_TIME, shop.getMinTimeToDeliver());

        // Inserting Row
        long result = db.insert(TABLE_SHOPS, null, values);
        db.close();
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Shop getShop(int shopId) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SHOPS, new String[]{KEY_SHOP_ID,
                        KEY_SHOP_NAME, KEY_SHOP_PHONE, KEY_SHOP_LATITUDE, KEY_SHOP_LONGITUDE, KEY_SHOP_MIN_ORDER, KEY_SHOP_MIN_DELIVERY_TIME, KEY_SHOP_IMAGE_URL_SMALL, KEY_SHOP_IMAGE_URL_LARGE}, KEY_SHOP_ID + "=?",
                new String[]{String.valueOf(shopId)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Shop shop = new Shop(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getDouble(3), cursor.getDouble(4), cursor.getDouble(5), cursor.getInt(6), cursor.getString(7), cursor.getString(8));
        db.close();
        return shop;
    }

    /* ----------------------------- FAVOURITES METHODS ------------------------------------------ */

    public boolean addToFavourites(FavouriteItem favouriteItem) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PRODUCT_ID, favouriteItem.getProductId());
        values.put(KEY_SHOP_ID, favouriteItem.getShopId());
        values.put(KEY_PRODUCT_NAME, favouriteItem.getProductName());
        values.put(KEY_PRODUCT_PRICE, favouriteItem.getProductPrice());

        // Inserting Row
        long result = db.insert(TABLE_FAVOURITES, null, values);
        db.close();
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public ArrayList<FavouriteItem> getFavouriteItems() {

        ArrayList<FavouriteItem> results;
        SQLiteDatabase db = this.getReadableDatabase();

        results = new ArrayList<FavouriteItem>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_FAVOURITES, null);
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            FavouriteItem temp;
            temp = new FavouriteItem(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getDouble(3), R.drawable.sample_product);
            results.add(temp);
        }
        db.close();
        return results;
    }

    public void deleteFromFavourites(FavouriteItem favouriteItem) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_FAVOURITES, KEY_PRODUCT_ID + " = ?",
                new String[]{String.valueOf(favouriteItem.getProductId())});
        db.close();
    }

    public int getFavouritesCount() {
        int count;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_FAVOURITES, null);
        if (cursor != null)
            cursor.moveToFirst();
        count = cursor.getInt(0);
        return count;
    }

    /* ----------------------------- OFFLINE PRODUCTS AND CATEGORY METHODS ------------------------------------------ */

    public boolean addOfflineShop(ArrayList<Category> categoryList, ArrayList<OfflineProduct> productsList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_OFFLINE_CATEGORIES);
        db.execSQL("DELETE FROM " + TABLE_OFFLINE_PRODUCTS);

        for (int i = 0; i < categoryList.size(); i++) {
            Category category = categoryList.get(i);
            ContentValues values = new ContentValues();
            values.put(KEY_CATEGORY_ID, category.categoryId);
            values.put(KEY_CATEGORY_NAME, category.title);
            long result = db.insert(TABLE_OFFLINE_CATEGORIES, null, values);
            if (result == -1) {
                db.close();
                return false;
            }
        }

        for (int i = 0; i < productsList.size(); i++) {
            OfflineProduct offlineProduct = productsList.get(i);
            ContentValues values = new ContentValues();
            values.put(KEY_PRODUCT_ID, offlineProduct.getProductId());
            values.put(KEY_CATEGORY_ID, offlineProduct.getCategoryId());
            values.put(KEY_SHOP_ID, offlineProduct.getShopId());
            values.put(KEY_PRODUCT_SHORT_NAME, offlineProduct.getProductShortName());
            values.put(KEY_PRODUCT_LONG_NAME, offlineProduct.getProductLongName());
            values.put(KEY_PRODUCT_DESCRIPTION, offlineProduct.getProductDescription());
            values.put(KEY_PRODUCT_PRICE, offlineProduct.getProductPrice().toDouble());

            values.put(KEY_PRODUCT_IMAGE_URL_SMALL, offlineProduct.getProductIconUrl());
            values.put(KEY_PRODUCT_IMAGE_URL_LARGE, offlineProduct.getProductImageUrl());

            long result = db.insert(TABLE_OFFLINE_PRODUCTS, null, values);
            if (result == -1) {
                db.close();
                return false;
            }
        }
        db.close();
        return true;
    }

    private boolean addOfflineCategory(Category category) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CATEGORY_ID, category.categoryId);
        values.put(KEY_CATEGORY_NAME, category.title);

        long result = db.insert(TABLE_OFFLINE_CATEGORIES, null, values);
        db.close();
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    private boolean addOfflineProduct(OfflineProduct offlineProduct) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PRODUCT_ID, offlineProduct.getProductId());
        values.put(KEY_CATEGORY_ID, offlineProduct.getCategoryId());
        Log.e("cat id", offlineProduct.getCategoryId() + "");
        values.put(KEY_SHOP_ID, offlineProduct.getShopId());
        values.put(KEY_PRODUCT_SHORT_NAME, offlineProduct.getProductShortName());
        values.put(KEY_PRODUCT_LONG_NAME, offlineProduct.getProductLongName());
        values.put(KEY_PRODUCT_DESCRIPTION, offlineProduct.getProductDescription());
        values.put(KEY_PRODUCT_PRICE, offlineProduct.getProductPrice().toDouble());
        values.put(KEY_SHOP_IMAGE_URL_SMALL, offlineProduct.getProductIconUrl());
        values.put(KEY_SHOP_IMAGE_URL_LARGE, offlineProduct.getProductImageUrl());

        long result = db.insert(TABLE_OFFLINE_PRODUCTS, null, values);
        db.close();
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public ArrayList<Category> getOfflineCategories() {

        ArrayList<Category> results;
        SQLiteDatabase db = this.getReadableDatabase();

        results = new ArrayList<Category>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_OFFLINE_CATEGORIES, null);
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            Category temp;
            temp = new Category(cursor.getInt(0), R.drawable.stop_shop_logo, cursor.getString(1));
            Log.e("cat id", temp.categoryId + "");
            results.add(temp);
        }
        db.close();
        return results;
    }

    public ArrayList<OfflineProduct> getOfflineProducts(int categoryId) {

        ArrayList<OfflineProduct> results;
        SQLiteDatabase db = this.getReadableDatabase();

        results = new ArrayList<OfflineProduct>();
        Cursor cursor = db.query(TABLE_OFFLINE_PRODUCTS, new String[]{KEY_PRODUCT_ID,
                        KEY_CATEGORY_ID, KEY_SHOP_ID, KEY_PRODUCT_SHORT_NAME, KEY_PRODUCT_LONG_NAME, KEY_PRODUCT_IMAGE_URL_SMALL, KEY_PRODUCT_IMAGE_URL_LARGE, KEY_PRODUCT_DESCRIPTION, KEY_PRODUCT_PRICE}, KEY_CATEGORY_ID + "=?",
                new String[]{String.valueOf(categoryId)}, null, null, null, null);
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            OfflineProduct temp;
            temp = new OfflineProduct(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), new UnitOfMeasurement("count", false, "value"), new ProductPrice(cursor.getDouble(8), "INR"));
            results.add(temp);
        }
        db.close();
        return results;
    }

    public OfflineProduct getOfflineProduct(int productId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_OFFLINE_PRODUCTS, new String[]{KEY_PRODUCT_ID,
                        KEY_CATEGORY_ID, KEY_SHOP_ID, KEY_PRODUCT_SHORT_NAME, KEY_PRODUCT_LONG_NAME, KEY_PRODUCT_IMAGE_URL_SMALL, KEY_PRODUCT_IMAGE_URL_LARGE, KEY_PRODUCT_DESCRIPTION, KEY_PRODUCT_PRICE}, KEY_PRODUCT_ID + "=?",
                new String[]{String.valueOf(productId)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        OfflineProduct offlineProduct = new OfflineProduct(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), new UnitOfMeasurement("count", false, "value"), new ProductPrice(cursor.getDouble(8), "INR"));
        db.close();
        return offlineProduct;
    }


    /* ----------------------------- HISTORY METHODS ------------------------------------------ */

    public boolean insertHistoryOrder(HistoryOrder historyOrder){
        addAddress(historyOrder.getShipToAddress());
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Item> items = historyOrder.getItems();
        for (Item temp : items) {
            ContentValues values = new ContentValues();
            values.put(KEY_ORDER_ID, historyOrder.getOrderId());
            values.put(KEY_SHOP_ID, historyOrder.getShopId());
            values.put(KEY_ORDER_DATE, historyOrder.getOrderedDate());
            values.put(KEY_DELIVERY_TIME, historyOrder.getDeliveryTime());
            values.put(KEY_PRODUCT_ID, temp.getProductId());
            values.put(KEY_STATUS, historyOrder.getStatus());
            values.put(KEY_ADDRESS_ID, historyOrder.getShipToAddress().getAddressId());
            values.put(KEY_PRODUCT_SHORT_NAME, temp.getProductShortName());
            values.put(KEY_PRODUCT_LONG_NAME, temp.getProductLongName());
            values.put(KEY_PRODUCT_PRICE, temp.getPrice().getActual());
            values.put(KEY_ITEM_STATUS, temp.getStatus());
            values.put(KEY_ORDER_ITEM_ID, temp.getOrderItemId());
            values.put(KEY_QUANTITY, temp.getQuantity());
            long result = db.insert(TABLE_HISTORY, null, values);
            if (result == -1) {
                db.close();
                return false;
            }
        }
        db.close();
        return true;
    }

    private boolean addAddress(Address address) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCursor = db.rawQuery("SELECT * FROM " + TABLE_ADDRESS + " WHERE " + KEY_ADDRESS_ID + "=?", new String[]{String.valueOf(address.getAddressId())});
        if(mCursor.getCount() <= 0){
            ContentValues values = new ContentValues();
            values.put(KEY_ADDRESS_ID, address.getAddressId());
            values.put(KEY_ADDRESS_LINE_1, address.getAddressLine1());
            values.put(KEY_ADDRESS_LINE_2, address.getAddressLine2());
            values.put(KEY_STREET, address.getStreet());
            values.put(KEY_CITY, address.getCity());
            values.put(KEY_STATE, address.getState());
            values.put(KEY_COUNTRY, address.getCountry());
            values.put(KEY_ZIP, address.getZip());
            long result = db.insert(TABLE_ADDRESS, null, values);
            db.close();
            if (result == -1) {
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    public HistoryOrder getOrder(int orderId){
        HistoryOrder historyOrder;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursorOrders = db.query(TABLE_HISTORY, new String[]{
                        KEY_ORDER_ID,
                        KEY_SHOP_ID,
                        KEY_ORDER_DATE,
                        KEY_DELIVERY_TIME,
                        KEY_STATUS,
                        KEY_ADDRESS_ID,
                        KEY_PRODUCT_ID,
                        KEY_PRODUCT_SHORT_NAME,
                        KEY_PRODUCT_LONG_NAME,
                        KEY_PRODUCT_PRICE,
                        KEY_ITEM_STATUS,
                        KEY_ORDER_ITEM_ID,
                        KEY_QUANTITY}, KEY_ORDER_ID + "=?",
                new String[]{String.valueOf(orderId)}, null, null, null, null);
        ArrayList<Item> itemList = new ArrayList<Item>();
        for (cursorOrders.moveToFirst(); !cursorOrders.isAfterLast(); cursorOrders.moveToNext()) {
            Item item = new Item(cursorOrders.getInt(6), cursorOrders.getInt(1), cursorOrders.getString(7), cursorOrders.getString(8), new ProductPrice(cursorOrders.getDouble(9)), cursorOrders.getString(10), "value", cursorOrders.getInt(11), cursorOrders.getInt(12));
            itemList.add(item);
        }
        cursorOrders.moveToFirst();
        int addressId = cursorOrders.getInt(5);
        Cursor cursorAddress = db.query(TABLE_ADDRESS, new String[]{
                        KEY_ADDRESS_ID,
                        KEY_ADDRESS_LINE_1,
                        KEY_ADDRESS_LINE_2,
                        KEY_STREET,
                        KEY_CITY,
                        KEY_CITY,
                        KEY_STATE,
                        KEY_COUNTRY,
                        KEY_ZIP}, KEY_ADDRESS_ID + "=?",
                new String[]{String.valueOf(addressId)}, null, null, null, null);
        cursorAddress.moveToFirst();
        Address address = new Address(cursorAddress.getString(1), cursorAddress.getString(2), cursorAddress.getString(3),cursorAddress.getString(4),cursorAddress.getString(5),cursorAddress.getString(6),cursorAddress.getString(7));
        cursorOrders.moveToFirst();
        historyOrder = new HistoryOrder(cursorOrders.getInt(0), cursorOrders.getInt(1), cursorOrders.getString(2), cursorOrders.getString(3), cursorOrders.getString(4), address, itemList);
        return historyOrder;
    }

    public ArrayList<HistoryOrder> getHistoryOrders() {

        ArrayList<HistoryOrder> results = new ArrayList<HistoryOrder>();
        ArrayList<Integer> orderIds = new ArrayList<Integer>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor1 = db.query(true, TABLE_HISTORY, new String[] { KEY_ORDER_ID }, null, null, KEY_ORDER_ID, null, null, null);
        for (cursor1.moveToFirst(); !cursor1.isAfterLast(); cursor1.moveToNext()) {
            orderIds.add(cursor1.getInt(0));
        }
        for (Integer temp : orderIds) {
            Cursor cursorOrders = db.query(TABLE_HISTORY, new String[]{
                            KEY_ORDER_ID,
                            KEY_SHOP_ID,
                            KEY_ORDER_DATE,
                            KEY_DELIVERY_TIME,
                            KEY_STATUS,
                            KEY_ADDRESS_ID,
                            KEY_PRODUCT_ID,
                            KEY_PRODUCT_SHORT_NAME,
                            KEY_PRODUCT_LONG_NAME,
                            KEY_PRODUCT_PRICE,
                            KEY_ITEM_STATUS,
                            KEY_ORDER_ITEM_ID,
                            KEY_QUANTITY}, KEY_ORDER_ID + "=?",
                    new String[]{String.valueOf(temp)}, null, null, null, null);
            ArrayList<Item> itemList = new ArrayList<Item>();
            for (cursorOrders.moveToFirst(); !cursorOrders.isAfterLast(); cursorOrders.moveToNext()) {
                Item item = new Item(cursorOrders.getInt(6), cursorOrders.getInt(1), cursorOrders.getString(7), cursorOrders.getString(8), new ProductPrice(cursorOrders.getDouble(9)), cursorOrders.getString(10), "value", cursorOrders.getInt(11), cursorOrders.getInt(12));
                itemList.add(item);
            }
            cursorOrders.moveToFirst();
            int addressId = cursorOrders.getInt(5);
            Cursor cursorAddress = db.query(TABLE_ADDRESS, new String[]{
                            KEY_ADDRESS_ID,
                            KEY_ADDRESS_LINE_1,
                            KEY_ADDRESS_LINE_2,
                            KEY_STREET,
                            KEY_CITY,
                            KEY_CITY,
                            KEY_STATE,
                            KEY_COUNTRY,
                            KEY_ZIP}, KEY_ADDRESS_ID + "=?",
                    new String[]{String.valueOf(addressId)}, null, null, null, null);
            cursorAddress.moveToFirst();
            Address address = new Address(cursorAddress.getString(1), cursorAddress.getString(2), cursorAddress.getString(3),cursorAddress.getString(4),cursorAddress.getString(5),cursorAddress.getString(6),cursorAddress.getString(7));
            cursorOrders.moveToFirst();
            results.add(new HistoryOrder(cursorOrders.getInt(0), cursorOrders.getInt(1), cursorOrders.getString(2), cursorOrders.getString(3), cursorOrders.getString(4), address, itemList));
        }
        db.close();
        return results;
    }


}
