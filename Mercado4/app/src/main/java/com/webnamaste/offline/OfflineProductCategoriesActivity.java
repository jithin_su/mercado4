package com.webnamaste.offline;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.webnamaste.adapters.CategoryAdapter;
import com.webnamaste.database.DatabaseHandler;
import com.webnamaste.mercado.NavigationDrawerFragment;
import com.webnamaste.mercado.ProductListActivity;
import com.webnamaste.mercado.R;
import com.webnamaste.pojo.Category;
import com.webnamaste.pojo.Shop;

import java.util.ArrayList;

public class OfflineProductCategoriesActivity extends AppCompatActivity {

    private Toolbar toolbar;
    GridView gvProductCategories;
    ProgressBar progressBar;
    ArrayList<Category> list;
    DatabaseHandler databaseHandler;
    Shop shop;
    TextView tvTitle;

    int shopId = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_product_categories);

        databaseHandler = new DatabaseHandler(getApplicationContext());
        shop = databaseHandler.getShop(shopId);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        NavigationDrawerFragment drawerFragment;
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);

        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setText(shop.getShopName());

        populateListView();

    }

    private void populateListView() {
        list = databaseHandler.getOfflineCategories();
        if(list.size()>0){
            gvProductCategories = (GridView) findViewById(R.id.gvProductCategories);
            gvProductCategories.setAdapter(new CategoryAdapter(getApplicationContext(), list));
            gvProductCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent shopsActivity = new Intent(OfflineProductCategoriesActivity.this, OfflineProductListActivity.class);
                    shopsActivity.putExtra("categoryId", list.get(position).categoryId);
                    shopsActivity.putExtra("shopId", shopId);
                    startActivity(shopsActivity);
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_offline_product_categories, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
