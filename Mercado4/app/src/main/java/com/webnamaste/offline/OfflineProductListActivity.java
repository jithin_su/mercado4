package com.webnamaste.offline;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.webnamaste.database.DatabaseHandler;
import com.webnamaste.mercado.NavigationDrawerFragment;
import com.webnamaste.mercado.ProductDetailsActivity;
import com.webnamaste.mercado.R;
import com.webnamaste.pojo.CartItem;
import com.webnamaste.pojo.FavouriteItem;
import com.webnamaste.pojo.OfflineProduct;
import com.webnamaste.pojo.Product2;
import com.webnamaste.singletons.StoriCart;
import com.webnamaste.singletons.VolleySingleton;

import java.util.ArrayList;

public class OfflineProductListActivity extends AppCompatActivity {

    private Toolbar toolbar;
    ListView lvProducts;
    ArrayList<OfflineProduct> list;

    TextView tvFavCount;

    TextView tvTitle;
    DatabaseHandler databaseHandler;
    TextView tvNoData;

    TextView tvQuantity;
    Button buttonPlus, buttonMinus;

    int categoryId,shopId;

    private RequestQueue requestQueue;
    private ImageLoader mImageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_product_list);

        categoryId = getIntent().getIntExtra("categoryId", 0);
        shopId = getIntent().getIntExtra("shopId", 0);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        NavigationDrawerFragment drawerFragment;
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);

        lvProducts = (ListView) findViewById(R.id.lvProducts);

        databaseHandler = new DatabaseHandler(getApplicationContext());

        list = databaseHandler.getOfflineProducts(categoryId);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setText(databaseHandler.getShop(list.get(1).getShopId()).getShopName());
        if(list.size()>0){
            lvProducts.setAdapter(new OfflineProductsAdapter(OfflineProductListActivity.this, list));
            lvProducts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent productDetailsActivity = new Intent(OfflineProductListActivity.this, OfflineProductDetailsActivity.class);
                    OfflineProduct selected = list.get(position);
                    productDetailsActivity.putExtra("productId", selected.getProductId());
                    productDetailsActivity.putExtra("shopId", selected.getShopId());
                    startActivity(productDetailsActivity);
                }
            });
        } else {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_offline_product_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class OfflineProductsAdapter extends BaseAdapter {

        ArrayList<OfflineProduct> list;
        Context context;
        DatabaseHandler databaseHandler;

        public OfflineProductsAdapter(Context context, ArrayList<OfflineProduct> list) {
            this.context = context;
            this.list = list;
            databaseHandler = new DatabaseHandler(context);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return list.get(position).getProductId();
        }

        /**
         * Class to hold the view of a single row view
         */
        class ViewHolder {

            NetworkImageView ivThumb;
            TextView tvTitle;
            TextView tvPrice;
            ImageView ivFav;
            ImageView ivCart;

            ViewHolder(View v) {
                ivThumb = (NetworkImageView) v.findViewById(R.id.thumb);
                tvTitle = (TextView) v.findViewById(R.id.tvTitle);
                tvPrice = (TextView) v.findViewById(R.id.tvPrice);
                ivFav = (ImageView) v.findViewById(R.id.ivFav);
                ivCart = (ImageView) v.findViewById(R.id.ivCart);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View row = convertView;
            ViewHolder holder = null;

            requestQueue = VolleySingleton.getInstance().getRequestQueue();
            mImageLoader = VolleySingleton.getInstance().getImageLoader();

            if (row == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.products_list_item, parent, false);
                holder = new ViewHolder(row);
                row.setTag(holder);
            } else {
                holder = (ViewHolder) row.getTag();
            }

            OfflineProduct temp = list.get(position);
            holder.ivThumb.setImageUrl(temp.getProductIconUrl(), mImageLoader);
            holder.tvTitle.setText(temp.getProductShortName());
            holder.tvPrice.setText(temp.getProductPrice().toString());
            holder.ivFav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OfflineProduct selected = list.get(position);
                    if (databaseHandler.addToFavourites(new FavouriteItem(selected.getProductId(), selected.getShopId(), selected.getProductShortName(), selected.getProductPrice().toDouble(), R.drawable.sample_product))) {
                        tvFavCount.setText(databaseHandler.getFavouritesCount() + "");
                        Toast.makeText(context, "Added to Favourites", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(StoriCart.getAppContext(), "Already in Favourites", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            holder.ivCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.product_popup);
                    Button buttonCancel = (Button) dialog.findViewById(R.id.buttonCancel);
                    buttonCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    tvQuantity = (TextView) dialog.findViewById(R.id.tvQuantity);
                    buttonPlus = (Button) dialog.findViewById(R.id.buttonPlus);
                    buttonPlus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int quantity = Integer.parseInt(tvQuantity.getText().toString());
                            quantity++;
                            tvQuantity.setText(Integer.toString(quantity));
                        }
                    });

                    buttonMinus = (Button) dialog.findViewById(R.id.buttonMinus);
                    buttonMinus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int quantity = Integer.parseInt(tvQuantity.getText().toString());
                            if(quantity >1) {
                                quantity--;
                                tvQuantity.setText(Integer.toString(quantity));
                            }
                        }
                    });
                    Button buttonAddToCart = (Button) dialog.findViewById(R.id.button_add_to_cart);
                    buttonAddToCart.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            OfflineProduct temp = list.get(position);
                            CartItem cartItem = new CartItem(temp.getProductId(), temp.getShopId(), temp.getProductShortName(), temp.getProductPrice().toDouble(), Integer.parseInt(tvQuantity.getText().toString()), R.drawable.sample_product);
                            if(databaseHandler.addToCart(cartItem)) {
                                Toast.makeText(getApplicationContext(), "Added to Cart", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Already in Cart", Toast.LENGTH_SHORT).show();
                            }
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            });
            return row;
        }
    }
}
