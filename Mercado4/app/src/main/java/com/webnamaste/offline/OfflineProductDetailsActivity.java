package com.webnamaste.offline;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.webnamaste.database.DatabaseHandler;
import com.webnamaste.mercado.CartActivity;
import com.webnamaste.mercado.NavigationDrawerFragment;
import com.webnamaste.mercado.R;
import com.webnamaste.pojo.CartItem;
import com.webnamaste.pojo.FavouriteItem;
import com.webnamaste.pojo.OfflineProduct;
import com.webnamaste.singletons.StoriCart;
import com.webnamaste.singletons.VolleySingleton;

public class OfflineProductDetailsActivity extends AppCompatActivity {

    private static final String RUPEE = "\u20B9 ";

    private Toolbar toolbar;
    Button addToCart, buyNow;
    ImageView ivFav;
    LinearLayout llDetails;
    ProgressBar progressBar;
    TextView tvFavCount;

    int productId, shopId;

    OfflineProduct offlineProduct;

    TextView tvTitle, tvShopName, tvDescription, tvQuantity, tvPrice;
    Button buttonAdd, buttonMinus;

    DatabaseHandler databaseHandler;

    NetworkImageView productImage;
    RequestQueue requestQueue;
    ImageLoader mImageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_product_details);

        productId = getIntent().getIntExtra("productId",0);
        shopId = getIntent().getIntExtra("shopId",0);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        NavigationDrawerFragment drawerFragment;
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);

        bindAll();

        databaseHandler = new DatabaseHandler(getApplicationContext());
        getProductDetails();

    }

    private void getProductDetails() {
        requestQueue = VolleySingleton.getInstance().getRequestQueue();
        mImageLoader = VolleySingleton.getInstance().getImageLoader();
        offlineProduct = databaseHandler.getOfflineProduct(productId);
        tvTitle.setText(offlineProduct.getProductLongName());
        tvDescription.setText("Description goes here");
        tvShopName.setText(databaseHandler.getShop(shopId).getShopName());
        tvPrice.setText("Price: " + RUPEE + offlineProduct.getProductPrice().toDouble());
        productImage.setImageUrl(offlineProduct.getProductImageUrl(), mImageLoader);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quantity = Integer.parseInt(tvQuantity.getText().toString());
                quantity++;
                tvQuantity.setText(Integer.toString(quantity));
            }
        });
        buttonMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quantity = Integer.parseInt(tvQuantity.getText().toString());
                if(quantity >1) {
                    quantity--;
                    tvQuantity.setText(Integer.toString(quantity));
                }
            }
        });
        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CartItem cartItem = new CartItem(offlineProduct.getProductId(), offlineProduct.getShopId(), offlineProduct.getProductShortName(), offlineProduct.getProductPrice().toDouble(), Integer.parseInt(tvQuantity.getText().toString()), R.drawable.sample_product);
                if(databaseHandler.addToCart(cartItem)) {
                    Toast.makeText(getApplicationContext(), "Added to Cart", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Already in Cart", Toast.LENGTH_SHORT).show();
                }
            }
        });
        buyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CartItem cartItem = new CartItem(offlineProduct.getProductId(), offlineProduct.getShopId(), offlineProduct.getProductShortName(), offlineProduct.getProductPrice().toDouble(), Integer.parseInt(tvQuantity.getText().toString()), R.drawable.sample_product);
                databaseHandler.addToCart(cartItem);
                Intent intent = new Intent(OfflineProductDetailsActivity.this, CartActivity.class);
                startActivity(intent);
            }
        });
        ivFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseHandler = new DatabaseHandler(StoriCart.getAppContext());
                if(databaseHandler.addToFavourites(new FavouriteItem(offlineProduct.getProductId(), offlineProduct.getShopId(), offlineProduct.getProductShortName(), offlineProduct.getProductPrice().toDouble(), R.drawable.sample_product))){
                    tvFavCount.setText(databaseHandler.getFavouritesCount() + "");
                    Toast.makeText(StoriCart.getAppContext(), "Added to Favourites", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(StoriCart.getAppContext(), "Already in Favourites", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void bindAll() {

        addToCart = (Button) findViewById(R.id.button_add_to_cart);
        ivFav = (ImageView) findViewById(R.id.ivFav);
        buyNow = (Button) findViewById(R.id.button_buy_now);
        llDetails = (LinearLayout) findViewById(R.id.llDetails);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvShopName = (TextView) findViewById(R.id.tvShopName);
        tvPrice = (TextView) findViewById(R.id.tvPrice);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
        tvQuantity = (TextView) findViewById(R.id.tvQuantity);
        buttonAdd = (Button) findViewById(R.id.buttonAdd);
        buttonMinus = (Button) findViewById(R.id.buttonMinus);
        productImage = (NetworkImageView) findViewById(R.id.ivProductImage);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_offline_product_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
